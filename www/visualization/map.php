<?php

$server = "localhost";
$user = "root";
$password = "D@T@f3st!!!";
//$password = "";
$database = "health24";

//***************************************************************************************
if ($_GET['id'] == 'ip') {
    $link = mysql_connect($server, $user, $password) or die('Could not connect: ' . mysql_error());
    mysql_select_db($database) or die('Could not select database');
    $query = 'select ip,latitude,longitude,
	group_concat(distinct b.language_name separator \', \') as source_language,
    group_concat(distinct c.language_name separator \', \') as target_language,
	group_concat(distinct dev.device_name  separator \', \') as device
    from visualization a 
    join lang_lookup b 
    on a.source_language = b.lang_id 
    join lang_lookup c 
    on  a.target_language = c.lang_id
    join device_lookup dev 
    on a.device = dev.device_id
    group by ip,latitude,longitude';

    $result = mysql_query($query) or die('Query failed: ' . mysql_error());
    $output = array(array());
    $i = 0;
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $j = 0;
        foreach ($line as $col_value) {
            $output[$i][$j] = $col_value;
            $j++;
        }
        $i++;
    }
    mysql_free_result($result);
    mysql_close($link);


    $fp = fopen('file/ipaddress.csv', 'w');
    fputcsv($fp, array("ip", "lat", "lon", "city", "state", "country", "source_language", "destination_language", "device", "comments")); //chr(9) is ASCII code for tab(\t)
    $output_array = array();
    for ($i = 0; $i < count($output); $i++) {
        $output_array = array();

        ini_set("allow_url_fopen", true);

        if ($output[$i][1] == null or $output[$i][1] == '255') {//lat lon in not provided
            $json = file_get_contents('http://ipinfo.io/' . $output[$i][0] . '/json');
            $expression = json_decode($json);
            $loc_pieces = explode(",", $expression->{"loc"});
            $output_array["ip"] = $expression->{"ip"};
            $output_array["lat"] = $loc_pieces[0];
            $output_array["lon"] = $loc_pieces[1];
            $output_array["city"] = $expression->{"city"};
            $output_array["state"] = $expression->{"region"};
            $output_array["country"] = $expression->{"country"};
            $output_array["source_language"] = $output[$i][3];
            $output_array["destination_language"] = $output[$i][4];
            $output_array["device"] = $output[$i][5];
            $output_array["comments"] = "Null lat lon in data";
            fputcsv($fp, $output_array);
        } else {//Lat lon is provided in data. Calculating other info based on lat lon
            $json = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng=' . $output[$i][1] . ',' . $output[$i][2] . '&sensor=false');
            $tmp = json_decode($json);
            $geo_info = $tmp->results[0]->address_components;
            $city = "";
            $county = "";
            $state = "";
            $country = "";
            for ($j = 0; $j < count($geo_info); $j++) {
                if ($geo_info[$j]->types[0] == "country")
                    $country = $geo_info[$j]->long_name;
                else if ($geo_info[$j]->types[0] == "administrative_area_level_1")
                    $state = $geo_info[$j]->long_name;
                else if ($geo_info[$j]->types[0] == "administrative_area_level_2")
                    $county = $geo_info[$j]->long_name;
                else if ($geo_info[$j]->types[0] == "locality")
                    $city = $geo_info[$j]->long_name;
            }
            if ($city == "")
                $city = $county;

            $output_array["ip"] = $output[$i][0];
            $output_array["lat"] = $output[$i][1];
            $output_array["lon"] = $output[$i][2];
            $output_array["city"] = $city;
            $output_array["state"] = $state;
            $output_array["country"] = $country;
            $output_array["source_language"] = $output[$i][3];
            $output_array["destination_language"] = $output[$i][4];
            $output_array["device"] = $output[$i][5];
            $output_array["comments"] = "Valid Lat lon provided";
            fputcsv($fp, $output_array);
        }
    }
    fclose($fp);
}
//***************************************************************************************
//*******************For Language *****************************
//SELECT target_language,count(*) FROM `visualization` group by target_language order by 1;

if ($_GET['id'] == 'language') {
    $link = mysql_connect($server, $user, $password) or die('Could not connect: ' . mysql_error());
    mysql_select_db($database) or die('Could not select database');
    $query = 'SELECT b.language_name as target_language,count(*) FROM `visualization` a
        join
        lang_lookup b
        on a.target_language = b.lang_id
        group by b.language_name';
    $result = mysql_query($query) or die('Query failed: ' . mysql_error());
    $languages = array(array());
    $i = 0;
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $j = 0;
        foreach ($line as $col_value) {
            $languages[$i][$j] = $col_value;
            $j++;
        }
        $i++;
    }
    mysql_free_result($result);
    mysql_close($link);

    $sum = 0;
    foreach ($languages as $language) {
        $sum+=$language[1];
    }
    for ($i = 0; $i < count($languages); $i++) {
        $languages[$i][1] = $languages[$i][1] / $sum;
    }
    $tsv = fopen('file/language.tsv', 'w');
    fputcsv($tsv, array("language", "frequency"), chr(9)); //chr(9) is ASCII code for tab(\t)
    foreach ($languages as $language) {
        fputcsv($tsv, $language, chr(9)); //chr(9) is ASCII code for tab(\t)
    }
    fclose($tsv);
}

//*******************For Language ***************************** 
//circle packing
if ($_GET['id'] == 'circle') {
    $link = mysql_connect($server, $user, $password) or die('Could not connect: ' . mysql_error());
    mysql_select_db($database) or die('Could not select database');
    $query = 'SELECT b.language_name as target_language,count(*) FROM `visualization` a
        join
        lang_lookup b
        on a.target_language = b.lang_id
        group by b.language_name';
    $result = mysql_query($query) or die('Query failed: ' . mysql_error());
    $languages = array(array());
    $i = 0;
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $j = 0;
        foreach ($line as $col_value) {
            $languages[$i][$j] = $col_value;
            $j++;
        }
        $i++;
    }
    mysql_free_result($result);
    mysql_close($link);

    $circle = fopen('file/circle.json', 'w');
    fwrite($circle, "{ \"name\": \"Language\", \"children\": [  ");
    $count = 0;
    foreach ($languages as $language) {
        if ($count != 0)
            fwrite($circle, ",");
        fwrite($circle, "{\"name\": \"" . $language[0] . "\", \"size\": " . $language[1] . "}");
        $count++;
    }
    fwrite($circle, "]}");
    fclose($circle);
}
?>

<?php
$subtitle = 'Visualization';
?>

<?php include ('../header.php'); ?>

<div style="text-align:center;">
  <a href="map.html">Geographic Metrics</a> | <a href="language.html">Language Metrics</a>| <a href="tree.html">Symptom Tree</a>
</div>

<?php include ('../footer.php'); ?>

<?php
/*
 _   _            _ _   _     ____  _  _   
| | | | ___  __ _| | |_| |__ |___ \| || |  
| |_| |/ _ \/ _` | | __| '_ \  __) | || |_ 
|  _  |  __/ (_| | | |_| | | |/ __/|__   _|
|_| |_|\___|\__,_|_|\__|_| |_|_____|  |_| 

UAH Computer Science Department
America's Datafest Global Competition
November 4, 2013 - November 10, 2013

Problem: Access to Health Information
Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible. 

Author: 
James Parkes, Mini Zeng

Team Members: 
Andrey Biglari, Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mini Zeng
*/

$alph = $_GET['alph'];
$url = 'drugbank_' . $alph . '.xml';
$xml = simplexml_load_file($url);
$numOfDrugs = $xml->drugs->drug->count();

$column1 = '';
$column2 = '';
$column3 = '';

for($i = 0; $i < $numOfDrugs; $i++) {
	$title=$xml->drugs->drug[$i]->name;
	$item_id=$xml->drugs->drug[$i]->drugbank_id;

	if($i % 3 == 0) {
		$column1 .= '<p><a href="medi_infor_detail.php?item_id=' . $item_id . '">' . $title . '</a></p>';
	} else if ($i % 3 == 1) {
		$column2 .= '<p><a href="medi_infor_detail.php?item_id=' . $item_id . '">' . $title . '</a></p>';
	} else {
		$column3 .= '<p><a href="medi_infor_detail.php?item_id=' . $item_id . '">' . $title . '</a></p>';
	}
}

$subtitle = 'Search Result for ' . $alph;
include ('../header.php');

?>

<div class="row medicine-row">
    <div class="col-lg-3"></div>
    <div class="col-lg-6">
    <?php include ('search-database.php'); ?>
    <?php include ('search-by-letter.php') ?>
    <h5>Search Result for <?php echo $alph; ?></h5>

	<div class="row">
		<div class="col-lg-4"><?php echo $column1; ?></div>
		<div class="col-lg-4"><?php echo $column2; ?></div>
		<div class="col-lg-4"><?php echo $column3; ?></div>
	</div>

<?php include ('../footer.php'); ?>

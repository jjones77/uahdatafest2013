<?php
/*
 _   _            _ _   _     ____  _  _   
| | | | ___  __ _| | |_| |__ |___ \| || |  
| |_| |/ _ \/ _` | | __| '_ \  __) | || |_ 
|  _  |  __/ (_| | | |_| | | |/ __/|__   _|
|_| |_|\___|\__,_|_|\__|_| |_|_____|  |_| 

UAH Computer Science Department
America's Datafest Global Competition
November 4, 2013 - November 10, 2013

Problem: Access to Health Information
Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible. 

Author: 
James Parkes, Mini Zeng

Team Members: 
Andrey Biglari, Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mini Zeng
*/

$url="drugbank_shortcut.xml";
$xml=simplexml_load_file($url);
$numOfDrugs = $xml->drugs->drug->count();
$target_id=$_GET['item_id'];

for($i = 0; $i < $numOfDrugs; $i++) {
    $title = $xml->drugs->drug[$i]->name;
    $item_id = $xml->drugs->drug[$i]->drugbank_id;

    if($item_id == $target_id){
        $name = $xml->drugs->drug[$i]->name;
        $indication = $xml->drugs->drug[$i]->indication;
        $description = $xml->drugs->drug[$i]->description;
        $pharmacology = $xml->drugs->drug[$i]->pharmacology;
    }
}

$subtitle = $name;
include ('../header.php');

?>

<div class="row medicine-row">
    <div class="col-lg-3"></div>
    <div class="col-lg-6">
        <?php include ('search-database.php'); ?>
        <?php include ('search-by-letter.php') ?>
        <div class="well well-sm">
        <h4><?php echo $name ?></h4>
        <p>
            <?php 
                echo '<h4 class="text-danger">Indication:</h4><p>';
                echo html_entity_decode($indication),'</p>';
                echo '<h4 class="text-info">Description:</h4><p>';
                echo html_entity_decode($description),'</p>';
                echo '<h4 class="text-info">Pharmacology:</h4></p>';
                echo html_entity_decode($pharmacology),'</p>';
            ?>
        </p>
    </div>
    <div class="col-lg-3"></div>
</div>

<?php include ('../footer.php'); ?>
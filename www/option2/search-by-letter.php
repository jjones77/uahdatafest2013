<?php
/*
 _   _            _ _   _     ____  _  _   
| | | | ___  __ _| | |_| |__ |___ \| || |  
| |_| |/ _ \/ _` | | __| '_ \  __) | || |_ 
|  _  |  __/ (_| | | |_| | | |/ __/|__   _|
|_| |_|\___|\__,_|_|\__|_| |_|_____|  |_| 

UAH Computer Science Department
America's Datafest Global Competition
November 4, 2013 - November 10, 2013

Problem: Access to Health Information
Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible. 

Author: 
James Parkes, Mini Zeng

Team Members: 
Andrey Biglari, Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mini Zeng
*/
?>

<h5>Browse Medicine Database Alphabetically</h5>
<div class="search-alpha">
	<a href="search_alph.php?alph=A" class="search-alpha-button"><button type="button" class="btn btn-default">A</button></a>
	<a href="search_alph.php?alph=B" class="search-alpha-button"><button type="button" class="btn btn-default">B</button></a>
	<a href="search_alph.php?alph=C" class="search-alpha-button"><button type="button" class="btn btn-default">C</button></a>
	<a href="search_alph.php?alph=D" class="search-alpha-button"><button type="button" class="btn btn-default">D</button></a>
	<a href="search_alph.php?alph=E" class="search-alpha-button"><button type="button" class="btn btn-default">E</button></a>
	<a href="search_alph.php?alph=F" class="search-alpha-button"><button type="button" class="btn btn-default">F</button></a>
	<a href="search_alph.php?alph=G" class="search-alpha-button"><button type="button" class="btn btn-default">G</button></a>
	<a href="search_alph.php?alph=H" class="search-alpha-button"><button type="button" class="btn btn-default">H</button></a>
	<a href="search_alph.php?alph=I" class="search-alpha-button"><button type="button" class="btn btn-default">I</button></a>
	<a href="search_alph.php?alph=J" class="search-alpha-button"><button type="button" class="btn btn-default">J</button></a>
	<a href="search_alph.php?alph=K" class="search-alpha-button"><button type="button" class="btn btn-default">K</button></a>
	<a href="search_alph.php?alph=L" class="search-alpha-button"><button type="button" class="btn btn-default">L</button></a>
	<a href="search_alph.php?alph=M" class="search-alpha-button"><button type="button" class="btn btn-default">M</button></a>
	<a href="search_alph.php?alph=N" class="search-alpha-button"><button type="button" class="btn btn-default">N</button></a>
	<a href="search_alph.php?alph=O" class="search-alpha-button"><button type="button" class="btn btn-default">O</button></a>
	<a href="search_alph.php?alph=P" class="search-alpha-button"><button type="button" class="btn btn-default">P</button></a>
	<a href="search_alph.php?alph=Q" class="search-alpha-button"><button type="button" class="btn btn-default">Q</button></a>
	<a href="search_alph.php?alph=R" class="search-alpha-button"><button type="button" class="btn btn-default">R</button></a>
	<a href="search_alph.php?alph=S" class="search-alpha-button"><button type="button" class="btn btn-default">S</button></a>
	<a href="search_alph.php?alph=T" class="search-alpha-button"><button type="button" class="btn btn-default">T</button></a>
	<a href="search_alph.php?alph=U" class="search-alpha-button"><button type="button" class="btn btn-default">U</button></a>
	<a href="search_alph.php?alph=V" class="search-alpha-button"><button type="button" class="btn btn-default">V</button></a>
	<a href="search_alph.php?alph=W" class="search-alpha-button"><button type="button" class="btn btn-default">W</button></a>
	<a href="search_alph.php?alph=X" class="search-alpha-button"><button type="button" class="btn btn-default">X</button></a>
	<a href="search_alph.php?alph=Y" class="search-alpha-button"><button type="button" class="btn btn-default">Y</button></a>
	<a href="search_alph.php?alph=Z" class="search-alpha-button"><button type="button" class="btn btn-default">Z</button></a>
</div>
<?php
/*
 _   _            _ _   _     ____  _  _   
| | | | ___  __ _| | |_| |__ |___ \| || |  
| |_| |/ _ \/ _` | | __| '_ \  __) | || |_ 
|  _  |  __/ (_| | | |_| | | |/ __/|__   _|
|_| |_|\___|\__,_|_|\__|_| |_|_____|  |_| 

UAH Computer Science Department
America's Datafest Global Competition
November 4, 2013 - November 10, 2013

Problem: Access to Health Information
Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible. 

Author: 
James Parkes, Mini Zeng

Team Members: 
Andrey Biglari, Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mini Zeng
*/

$error = '';
$sourceLanguage = $_POST['source-language'];
$languageCode = $_COOKIE['language'];
$imagePath = $_POST['final-image'];

$output = shell_exec('/bin/casperjs /uahdatafest2013/phantom/health24v3.js ' . $sourceLanguage . ' ' . $languageCode . ' /uahdatafest2013/www/option3/' . $_POST['final-image']);

// delete the image file
unlink('/uahdatafest2013/www/option3/' . $_POST['final-image']);

if($output == NULL) {
	$error = 'Error: Image could not be translated';
}

// set variables for database insert
$src = $sourceLanguage;
$dst = $languageCode;
$device = "0";
$lat = "255";
$lon = "255";
$ip = $_SERVER['REMOTE_ADDR'];

include ('../database-insert.php');
include ('../header.php');

?>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Translation</h4>
			</div>
			<div class="modal-body">
	        	<?php
					if ($error != '') {
						echo $error;
					} else {
						echo $output;
					}
				?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-lg-3"></div>
    <div class="col-lg-6">
    	<p>Translation:</p>
        <p>
        	<?php
				if ($error != '') {
					echo $error;
				} else {
					echo $output;
				}
			?>
        </p>
        <a href="/option3/" class="btn-link"><button class="btn btn-danger center-btn">Try Again?</button></a>
    </div>
    <div class="col-lg-3"></div>
</div>

<?php include ('footer.php'); ?>

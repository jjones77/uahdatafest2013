<?php
/*
 _   _            _ _   _     ____  _  _   
| | | | ___  __ _| | |_| |__ |___ \| || |  
| |_| |/ _ \/ _` | | __| '_ \  __) | || |_ 
|  _  |  __/ (_| | | |_| | | |/ __/|__   _|
|_| |_|\___|\__,_|_|\__|_| |_|_____|  |_| 

UAH Computer Science Department
America's Datafest Global Competition
November 4, 2013 - November 10, 2013

Problem: Access to Health Information
Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible. 

Author: 
James Parkes, Mini Zeng

Team Members: 
Andrey Biglari, Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mini Zeng
*/

function uploadImageFile() {
    try
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $iWidth = (int)$_POST['w'];
            $iHeight = (int)$_POST['h'];
            $iJpgQuality = 90;

            if ($_FILES) {

                // if no errors and size less than 250kb
                if (! $_FILES['image_file']['error'] && $_FILES['image_file']['size'] < 250 * 1024) {
                    // if file has been successfully uploaded
                    if (is_uploaded_file($_FILES['image_file']['tmp_name'])) {

                        // new unique filename
                        $sTempFileName =  '../tmp/' . md5(time().rand());

                        // move uploaded file into tmp folder
                        move_uploaded_file($_FILES['image_file']['tmp_name'],$sTempFileName);

                        // change file permission to 644
                        @chmod($sTempFileName, 0644);

                        if (file_exists($sTempFileName) && filesize($sTempFileName) > 0) {
                            $aSize = getimagesize($sTempFileName); // try to obtain image info

                            if (!$aSize) {
                                @unlink($sTempFileName);
                                return;
                            }

                            // check for image type
                            switch($aSize[2]) {
                                case IMAGETYPE_JPEG:
                                    $sExt = '.jpg';

                                    // create a new image from file
                                    $vImg = @imagecreatefromjpeg($sTempFileName);
                                    break;
                                case IMAGETYPE_PNG:
                                    $sExt = '.png';

                                    // create a new image from file
                                    $vImg = @imagecreatefrompng($sTempFileName);
                                    break;
                                case IMAGETYPE_GIF:
                                    $sExt = '.png';

                                    // create a new image from file
                                    $vImg = @imagecreatefromgif($sTempFileName);
                                    break;
                                default:
                                    @unlink($sTempFileName);
                                    return;
                            }

                            // create a new true color image
                            $vDstImg = @imagecreatetruecolor( $iWidth, $iHeight );

                            // copy and resize part of an image with resampling
                            imagecopyresampled($vDstImg, $vImg, 0, 0, (int)$_POST['x1'], (int)$_POST['y1'], $iWidth, $iHeight, (int)$_POST['w'], (int)$_POST['h']);

                            // define a result image filename
                            $sResultFileName = $sTempFileName . $sExt;

                            // output image to file
                            imagejpeg($vDstImg, $sResultFileName, $iJpgQuality);
                            @unlink($sTempFileName);

                            return $sResultFileName;
                        }
                    }
                }
            }
        }
    } catch(Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
    }
}

try {
   $sImage = uploadImageFile();
} catch(Exception $e) {
  echo 'Caught exception: ',  $e->getMessage(), "\n";
}

$subtitle = 'Translate Medicine - Confirm Image';
include ('../header.php');

?>

<form action="translate.php" method="POST" enctype="multipart/form-data" id="translate-form" name="translate-form" class="translate-form" role="form">
    <?php echo '<img src="' . $sImage . '" style="display:block;margin:0 auto;border:1px solid #333;"/>'; ?>
    <label class="source-language-label" for="source-language">Choose Source Language of Image</label>
    <select id="source-language" name="source-language" class="language-selector">
        <option value="af">akrikaans</option>
        <option value="sq">albanian</option>
        <option value="ar">arabic</option>
        <option value="az">azerbaijani</option>
        <option value="eu">basque</option>
        <option value="be">belarusian</option>
        <option value="bn">bengali</option>
        <option value="bg">bulgarian</option>
        <option value="ca">catalan</option>
        <option value="zn-CH">chinese (simplified)</option>
        <option value="zn-TW">chinese (traditional)</option>
        <option value="hr">croatian</option>
        <option value="cs">czech</option>
        <option value="da">danish</option>
        <option value="nl">dutch</option>
        <option value="en" selected="selected">english</option>
        <option value="eo">esperanto</option>
        <option value="et">estonian</option>
        <option value="fi">finnish</option>
        <option value="fr">french</option>
        <option value="gl">galician</option>
        <option value="de">german</option>
        <option value="el">greek</option>
        <option value="iw">hebrew</option>
        <option value="hi">hindi</option>
        <option value="hu">hungarian</option>
        <option value="is">icelandic</option>
        <option value="id">indonesian</option>
        <option value="it">italian</option>
        <option value="ja">japanese</option>
        <option value="kn">kannada</option>
        <option value="ko">korean</option>
        <option value="la">latin</option>
        <option value="lv">latvian</option>
        <option value="lt">lithuanian</option>
        <option value="mk">macedonian</option>
        <option value="ms">malay</option>
        <option value="mt">maltese</option>
        <option value="no">norwegian</option>
        <option value="pl">polish</option>
        <option value="pt">portuguese</option>
        <option value="ro">romanian</option>
        <option value="ru">russian</option>
        <option value="sr">serbian</option>
        <option value="sk">slovak</option>
        <option value="sl">slovenian</option>
        <option value="es">spanish</option>
        <option value="sw">swahili</option>
        <option value="sv">swedish</option>
        <option value="ta">tamil</option>
        <option value="te">telugu</option>
        <option value="th">thai</option>
        <option value="tr">turkish</option>
        <option value="uk">ukrainian</option>
        <option value="vi">vietnamese</option>          
    </select>
    <?php echo '<input type="hidden" name="final-image" id="final-image" value="' . $sImage . '">'; ?>
    <button type="submit" id="image-btn" name="image-btn" class="btn btn-primary center-btn">Translate</button>
</form>

<?php include ('../footer.php'); ?>

<?php
/*
 _   _            _ _   _     ____  _  _   
| | | | ___  __ _| | |_| |__ |___ \| || |  
| |_| |/ _ \/ _` | | __| '_ \  __) | || |_ 
|  _  |  __/ (_| | | |_| | | |/ __/|__   _|
|_| |_|\___|\__,_|_|\__|_| |_|_____|  |_| 

UAH Computer Science Department
America's Datafest Global Competition
November 4, 2013 - November 10, 2013

Problem: Access to Health Information
Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible. 

Author: 
James Parkes, Mini Zeng

Team Members: 
Andrey Biglari, Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mini Zeng
*/

$subtitle = 'Select/Upload Image';
include ('../header.php');

?>

<!-- upload form -->
<form id="upload_form" enctype="multipart/form-data" method="POST" action="confirm-image.php" onsubmit="return checkForm()">

    <!-- hidden crop params (corner positions) -->
    <input type="hidden" id="x1" name="x1" />
    <input type="hidden" id="y1" name="y1" />
    <input type="hidden" id="x2" name="x2" />
    <input type="hidden" id="y2" name="y2" />

    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <!-- display error message -->
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <div class="error"></div>
            </div>
        </div>
        <div class="col-lg-3"></div>
    </div>

    <!-- step 1 -->
    <h5>Step 1: Please select an image file</h5>
    <div>
		<input type="text" class="file-input-textbox" readonly="readonly" value="No File Selected">
		<div class="file-input-div">
		    <input type="button" id="image_file_button" class="image-file-button btn btn-primary btn-default" value="Browse" />
		    <input type="file" id="image_file" name="image_file" class="file-input-hidden" onchange="fileSelectHandler()" />
		</div>
	</div>

    <!-- step 2 -->
    <div class="step2">
        <h5>Step 2: Please select a crop region</h5>
        <img id="preview" />

		<!-- hidden crop params (crop image details) -->
        <input type="hidden" id="filesize" name="filesize" />
        <input type="hidden" id="filetype" name="filetype" />
        <input type="hidden" id="filedim" name="filedim" />
        <input type="hidden" id="w" name="w" />
        <input type="hidden" id="h" name="h" />

        <input type="submit" class="btn btn-primary btn-default" id="image-btn" name="image-btn" value="Upload" />
    </div>
</form>


<?php include ('../footer.php'); ?>

<?php
/*
 _   _            _ _   _     ____  _  _   
| | | | ___  __ _| | |_| |__ |___ \| || |  
| |_| |/ _ \/ _` | | __| '_ \  __) | || |_ 
|  _  |  __/ (_| | | |_| | | |/ __/|__   _|
|_| |_|\___|\__,_|_|\__|_| |_|_____|  |_| 

UAH Computer Science Department
America's Datafest Global Competition
November 4, 2013 - November 10, 2013

Problem: Access to Health Information
Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible. 

Author: 
James Parkes, Mini Zeng

Team Members: 
Andrey Biglari, Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mini Zeng
*/

$subtitle = 'Choose your Symptom';
include ('../header.php');

?>

<div class="row medicine-row">
	<div class="col-lg-3"></div>
	<div class="col-lg-6">
		<div class="col-sm-6 col-md-4">
		  	<a href="runny-nose.php" class="symptom-image">
			    <div class="thumbnail">
					<img src="/img/runny-nose.png">
					<div class="caption">
						<p>Runny Nose</p>
					</div>
			    </div>
			</a>
		</div>
  		<div class="col-sm-6 col-md-4">
  			<a href="headache.php" class="symptom-image">
    			<div class="thumbnail">
      				<img src="/img/headache.png">
					<div class="caption">
						<p>Headache</p>
					</div>
    			</div>
    		</a>
  		</div>
  		<div class="col-sm-6 col-md-4">
  			<a href="fever.php" class="symptom-image">
    			<div class="thumbnail">
      				<img src="/img/fever.png">
					<div class="caption">
						<p>Fever</p>
					</div>
    			</div>
    		</a>
  		</div>
  		<div class="col-sm-6 col-md-4">
  			<a href="sore-neck.php" class="symptom-image">	
   				<div class="thumbnail">
					<img src="/img/sore-neck.png">
					<div class="caption">
						<p>Sore Neck</p>
					</div>
    			</div>
    		</a>
  		</div>
	</div>
	<div class="col-lg-3"></div>
</div>

<?php include ('../footer.php'); ?>

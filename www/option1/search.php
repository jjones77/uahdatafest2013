<?php
/*
 _   _            _ _   _     ____  _  _   
| | | | ___  __ _| | |_| |__ |___ \| || |  
| |_| |/ _ \/ _` | | __| '_ \  __) | || |_ 
|  _  |  __/ (_| | | |_| | | |/ __/|__   _|
|_| |_|\___|\__,_|_|\__|_| |_|_____|  |_| 

UAH Computer Science Department
America's Datafest Global Competition
November 4, 2013 - November 10, 2013

Problem: Access to Health Information
Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible. 

Author: 
James Parkes, Mini Zeng

Team Members: 
Andrey Biglari, Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mini Zeng
*/
?>

<form class="search-box" id="tfnewsearch" method="GET" action="http://www.google.com" role="search">
	<div class="input-group col-sm-6">
		<input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
		<div class="input-group-btn">
			<button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
		</div>
	</div>
</form>
<?php
/*
 _   _            _ _   _     ____  _  _   
| | | | ___  __ _| | |_| |__ |___ \| || |  
| |_| |/ _ \/ _` | | __| '_ \  __) | || |_ 
|  _  |  __/ (_| | | |_| | | |/ __/|__   _|
|_| |_|\___|\__,_|_|\__|_| |_|_____|  |_| 

UAH Computer Science Department
America's Datafest Global Competition
November 4, 2013 - November 10, 2013

Problem: Access to Health Information
Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible. 

Author: 
James Parkes, Mini Zeng

Team Members: 
Andrey Biglari, Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mini Zeng
*/

$subtitle = 'Medicine for Runny Nose';
include ('../header.php'); 

?>
  	
<div class="row medicine-row">
	<div class="col-lg-3"></div>
	<div class="col-lg-6">
		<?php include ('search.php'); ?>
		<h5>Top 10 Medicines for Runny Nose</h5>
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
					<th>#</th>
					<th>Title</th>
				</tr>
			</thead>
			<tbody>
				<?php
					$url="runny_nose.xml";
					$xml=simplexml_load_file($url);

					for($i = 1; $i < 11; $i++) {
						$title = $xml->items->item[$i-1]->name;
						$item_id = $xml->items->item[$i-1]->itemId;

						echo 	'<tr>
									<td>' . $i . '</td>
									<td><a href="medi_infor_runny_nose.php?item_id=' . $item_id . '">' . $title . '</a></td>
								</tr>';
					}
				?>
			</tbody>
		</table>
	</div>
	<div class="col-lg-3"></div>
</div>

<?php include ('../footer.php'); ?>

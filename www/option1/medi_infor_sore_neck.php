<?php
/*
 _   _            _ _   _     ____  _  _   
| | | | ___  __ _| | |_| |__ |___ \| || |  
| |_| |/ _ \/ _` | | __| '_ \  __) | || |_ 
|  _  |  __/ (_| | | |_| | | |/ __/|__   _|
|_| |_|\___|\__,_|_|\__|_| |_|_____|  |_| 

UAH Computer Science Department
America's Datafest Global Competition
November 4, 2013 - November 10, 2013

Problem: Access to Health Information
Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible. 

Author: 
James Parkes, Mini Zeng

Team Members: 
Andrey Biglari, Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mini Zeng
*/

$url='sore_neck.xml';
$xml=simplexml_load_file($url);
$target_id=$_GET['item_id'];

for($i = 0; $i < 10; $i++) {
  $item_id = $xml->items->item[$i]->itemId;

  if($item_id == $target_id){
    $name = $xml->items->item[$i]->name;
    $description = $xml->items->item[$i]->longDescription;
    $category = $xml->items->item[$i]->categoryPath;
    $categories = split(' ', $category);
    $image = $xml->items->item[$i]->thumbnailImage;
    $walmart_url = $xml->items->item[$i]->productUrl;
  }
}

$subtitle = $name;
include ('../header.php');

?>

<div class="row medicine-row">
    <div class="col-lg-3"></div>
    <div class="col-lg-6">
        <?php include ('search.php'); ?>
        <h5>Medicine Information</h5>
        <div class="well well-sm">
            <div class="media">
                <?php
                
                  echo  '<a class="thumbnail pull-left" href="#">
                            <img class="media-object" src="' . $image . '">
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading">' . $name . '</h4>
                            <p>';

                            foreach($categories as $value) {
                                if($value == '&') {
                                    // do nothing, skip
                                } else {
                                    echo '<span class="label label-primary">' . $value . '</span>';
                                }
                            }

                  echo      '</p>
                            <p class="medicine-description">
                                <a href="' . $walmart_url . '" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-shopping-cart"></span> Purchase from Wal-Mart</a>
                            </p>
                            <p>' . html_entity_decode($description) . '</p>
                        </div>';

                ?>     
            </div>
        </div>
    </div>
    <div class="col-lg-3"></div>
</div>

<?php include ('../footer.php'); ?>

<?php
/*
 _   _            _ _   _     ____  _  _   
| | | | ___  __ _| | |_| |__ |___ \| || |  
| |_| |/ _ \/ _` | | __| '_ \  __) | || |_ 
|  _  |  __/ (_| | | |_| | | |/ __/|__   _|
|_| |_|\___|\__,_|_|\__|_| |_|_____|  |_| 

UAH Computer Science Department
America's Datafest Global Competition
November 4, 2013 - November 10, 2013

Problem: Access to Health Information
Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible. 

Author: 
James Parkes, Mini Zeng

Team Members: 
Andrey Biglari, Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mini Zeng
*/
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Health24 - <?php echo $subtitle; ?></title>

		<!-- Meta -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
		<link rel="shortcut icon" href="/favicon.ico">
		
		<!-- Stylesheets -->
		<link href="/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="/css/custom.css" rel="stylesheet" type="text/css">
		<link href="/css/jquery.Jcrop.min.css" rel="stylesheet" type="text/css">
	
	</head>	
	<body cz-shortcut-listen="true">
		<div class="container header-container">
			<a href="http://migrant.health24.net/"><img src="/img/health-24-logo.png" class="header-img"></a>
			<p style="margin:20px 0;text-align:center;">
				<a href="http://www.americas.datafest.net/" target="_blank">America's Datafest</a> | November 1-3, 2013<br>
				<a href="/option.php">Home</a> | <a href="/index.php">Pick Language</a> | <a href="/about.php">About</a> | <a href="https://bitbucket.org/jjones77/uahdatafest2013">Source Repository<a/>  | <a href="/visualization">Visualization</a><br>
				Email: <a href="mailto:migrant.health24.net@gmail.com">migrant.health24.net@gmail.com</a> 
			</p>
		</div>
		<div class="container">

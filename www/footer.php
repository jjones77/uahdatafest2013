<?php
/*
 _   _            _ _   _     ____  _  _   
| | | | ___  __ _| | |_| |__ |___ \| || |  
| |_| |/ _ \/ _` | | __| '_ \  __) | || |_ 
|  _  |  __/ (_| | | |_| | | |/ __/|__   _|
|_| |_|\___|\__,_|_|\__|_| |_|_____|  |_| 

UAH Computer Science Department
America's Datafest Global Competition
November 4, 2013 - November 10, 2013

Problem: Access to Health Information
Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible. 

Author: 
James Parkes, Mini Zeng

Team Members: 
Andrey Biglari, Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mini Zeng
*/
?>

		</div>
		<div id="google_translate_element"></div>
		<script type="text/javascript"> function googleTranslateElementInit() { new google.translate.TranslateElement({pageLanguage: 'en', autoDisplay: true, layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element'); } </script>
		<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
		<script src="http://code.jquery.com/jquery.js"></script>
		<script src="/js/bootstrap.min.js"></script>
		<script src="/js/jquery.Jcrop.min.js"></script>
		<script src="/js/image-upload.js"></script>
		<script type="text/javascript">
		    $(window).load(function(){
		        $('#myModal').modal('show');
		    });
		</script>
	</body>
</html>



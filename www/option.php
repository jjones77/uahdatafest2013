<?php
/*
 _   _            _ _   _     ____  _  _   
| | | | ___  __ _| | |_| |__ |___ \| || |  
| |_| |/ _ \/ _` | | __| '_ \  __) | || |_ 
|  _  |  __/ (_| | | |_| | | |/ __/|__   _|
|_| |_|\___|\__,_|_|\__|_| |_|_____|  |_| 

UAH Computer Science Department
America's Datafest Global Competition
November 4, 2013 - November 10, 2013

Problem: Access to Health Information
Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible. 

Author: 
James Parkes, Mini Zeng

Team Members: 
Andrey Biglari, Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mini Zeng
*/

$language = $_POST['language'];
setcookie("language", $language, time()+36000);

setcookie('googtrans', '/en/'.$language, time()+3600);
setcookie('googtrans', '/en/'.$language, time()+3600,'/','migrant.health24.net');
setcookie('googtrans', '/en/'.$language, time()+3600,'/','health24.net');

$subtitle = 'Welcome';
include ('header.php');

?>

<div class="row option-row">
	<div class="col-sm-6 col-md-4">
		<div class="thumbnail">
			<img src="img/symptom-man.png">
			<div class="caption">
				<a href="/option1/" class="option-link">
					<button class="btn btn-primary center-btn">Select Symptom</button>
				</a>
			</div>
		</div>
	</div>
	<div class="col-sm-6 col-md-4">
		<div class="thumbnail">
			<img src="img/medicine-bottle.png">
			<div class="caption">
				<a href="/option2/" class="option-link">
					<button class="btn btn-success center-btn">Find Medicine</button>
				</a>
			</div>
		</div>
	</div>
	<div class="col-sm-6 col-md-4">
		<div class="thumbnail">
			<img src="img/ocr-photo.png">
			<div class="caption">
				<a href="/option3/" class="option-link">
					<button class="btn btn-danger center-btn">Translate Medicine</button>
				</a>
			</div>
		</div>
	</div>
</div>

<?php include ('footer.php'); ?>

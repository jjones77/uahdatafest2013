<?php
/*
 _   _            _ _   _     ____  _  _   
| | | | ___  __ _| | |_| |__ |___ \| || |  
| |_| |/ _ \/ _` | | __| '_ \  __) | || |_ 
|  _  |  __/ (_| | | |_| | | |/ __/|__   _|
|_| |_|\___|\__,_|_|\__|_| |_|_____|  |_| 

UAH Computer Science Department
America's Datafest Global Competition
November 4, 2013 - November 10, 2013

Problem: Access to Health Information
Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible. 

Author: 
James Parkes, Mini Zeng

Team Members: 
Andrey Biglari, Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mini Zeng
*/

$subtitle = 'About';
include ('header.php');

?>

<div class="row about-row">
	<div class="col-lg-6">
		<div class="row">
			<div class="col-lg-12">
				<h4>Project Description</h4>
				<hr style="margin:8px auto">
				<p>Health24 is a healthcare application tailored for migrants. It is available for the Web, iOS, and Android platforms, and it has two major features: (1) diagnosis and (2) image-text translation. The primary goal of Health24 may be described as follows: "help migrants receive basic diagnoses and correctly identify and use over-the-counter (OTC) drugs to treat symptoms."</p>
			</div>
			<div class="col-lg-12" style="margin-top:40px;">
				<h4>Project Summary</h4>
				<hr style="margin:8px auto">
				<p>The Health24 team determined the functional pieces for a typical use case. A user is feeling sick and needs to determine what medicines to take. For that they use the symptom checker. Secondly, after knowing what medicine to take, the user would want to view the information about the drug in their native language to see how to take, what not to mix it with and so on. After they know info about the drug to take, they would go to buy the medicine. They can then take pictures of the medcine information and have the image converted to text and translated to their native language all in one step.<br><br> Before starting dvelopment we setup a BitBucket Git repository for all of our team code. We also setup a Debian Linux server to house the project, installing apache, configuring the firewall, and linking it to our repository. We installed the MySQL database for storing all of the site metrics. We then installed all of the software packages needed for the project.<br><br>We initially focused most of our work on the optical character recognition and language translation, as that was the most technically challenging. Since neither of these components existed together, phantomJS was chosen as a middle layer for combining the two components. A phantomJS script was created that leveraged a <a href="https://code.google.com/p/tesseract-ocr/">Tesseract OCR Engine</a> and <a href="http://translate.google.com/">Google Translate</a>. The script was able to process an image in English and translate the text to a target language. Then, we first focused on the web interface, as that would make this capability available to the widest audience. We accomplished this task by having a php page call the PhantomJS script in the background.<br><br> After creating the web interface we wanted to support different devices, so we created a network service (in Java) that any device could connect to in order to send a picture and the desired output language. Then, we created apps for the iOS and Android.<br><br>Simultanesously to the development of the mobile apps, one of the developers began working on the about page while another worked on the database and the metrics visualizations, creating the dynamic charts for the metrics that will be tracked by the service. We decided we did not have time to complete the first 2 steps before the deadline, so we instead stubbed in step 1 as more of a proof-of-concept.  We embedded a  Google Translate Javascript to the site enabling translating every page to any language, though in the future we plan to have the pages automatically translated to the user's selected language without further necessary actions.</p>
			</div>
			<div class="col-lg-12" style="margin-top:40px;">
				<h4>Future Plans</h4>
				<hr style="margin:8px auto">
				<ul>
					<li class="done">complete step1 (Diagnosis) and step2 (Drug database)</li>
					<li class="done">have complete site automatically translated to users selected language</li>
					<li>diagnosis feature for mobile applications</li>
					<li>provide diagnoses for more symptoms</li>
					<li class="done">collect metrics for data visualization</li>
					<li>improve speed of OCR (move Tesseract-OCR to local server)</li>
					<li class="done">add feature to view and crop image on web</li>
					<li class="done">ability to rotate image on mobile/tablet devices</li>
					<li>use stable cropping technique on android devices</li>
					<li>ability to use photos on android device</li>
					<li>consistent user experience on all platforms</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="team-members">
			<h4>Team Members</h4>
			<hr style="margin:8px auto">
		</div>
		<div class="col-lg-10" style="margin:20px auto;">
			<div class="media">
				<a class="pull-left" href="#">
					<img class="media-object dp img-circle" src="/img/jjones.jpg" style="height:130px;">
				</a>
				<div class="media-body">
					<h4 class="media-heading">Josh Jones <small> USA</small></h4>
					<span class="label label-default">java</span>
					<span class="label label-default">apache</span>
					<span class="label label-default">git</span>
					<span class="label label-default">javascript</span>
					<span class="label label-default">php</span>
					<ul style="margin-top:15px;">
						<li>server/domain setup</li>
						<li>git configuration</li>
						<li>java connection service</li>
						<li>java test client</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="col-lg-10" style="margin:20px auto;">
			<div class="media">
				<a class="pull-left" href="#">
					<img class="media-object dp img-circle" src="/img/mz0005.jpg" style="height:130px;">
				</a>
				<div class="media-body">
					<h4 class="media-heading">Mini Zeng <small> China</small></h4>
					<span class="label label-default">html5/css3</span>
					<span class="label label-default">javascript</span>
					<span class="label label-default">php</span>				
					<ul style="margin-top:15px;">
						<li>web design and UX</li>
						<li>symptoms web form</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-lg-10" style="margin:20px auto;">
			<div class="media">
				<a class="pull-left" href="#">
					<img class="media-object dp img-circle" src="/img/pj0007.jpg" style="height:130px;">
				</a>
				<div class="media-body">
					<h4 class="media-heading">Prabhash Jha <small> Nepal</small></h4>
					<span class="label label-default">html5/css3</span>
					<span class="label label-default">d3</span>
					<span class="label label-default">javascript</span>
					<span class="label label-default">php</span>
					<ul style="margin-top:15px;">
						<li>data visualizations</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-lg-10" style="margin:20px auto;">
			<div class="media">
				<a class="pull-left" href="#">
					<img class="media-object dp img-circle" src="/img/hhg0002.jpg" style="height:130px;">
				</a>
				<div class="media-body">
					<h4 class="media-heading">Ha Giang <small> Vietnam</small></h4>
					<span class="label label-default">java</span>
					<span class="label label-default">xml</span>
					<ul style="margin-top:15px;">
						<li>android mobile app</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="col-lg-10" style="margin:20px auto;">
			<div class="media">
				<a class="pull-left" href="#">
					<img class="media-object dp img-circle" src="/img/ajb0004.jpg" style="height:130px;">
				</a>
				<div class="media-body">
					<h4 class="media-heading">Andrey Biglari <small> Armenia</small></h4>
					<span class="label label-default">html5/css3</span>
					<span class="label label-default">javascript</span>
					<ul style="margin-top:15px;">
						<li>translation API</li>
					</ul>
				</div>
			</div>
		</div>


		<div class="col-lg-10" style="margin:20px auto;">
			<div class="media">
				<a class="pull-left" href="#">
					<img class="media-object dp img-circle" src="/img/jfp0005.jpg" style="height:130px;">
				</a>
				<div class="media-body">
					<h4 class="media-heading">Jarrod Parkes <small> USA</small></h4>
					<span class="label label-default">javascript</span>
					<span class="label label-default">phantomJS</span>
					<span class="label label-default">casperJS</span>
					<span class="label label-default">objective-C</span>
					<span class="label label-default">php</span>
					<ul style="margin-top:15px;">
						<li>phantomJS/casperJS glue layer</li>
						<li>iOS mobile app</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="col-lg-10" style="margin:20px auto;">
			<div class="media">
				<a class="pull-left" href="#">
					<img class="media-object dp img-circle" src="/img/jrp0007.jpg" style="height:130px;">
				</a>
				<div class="media-body">
					<h4 class="media-heading">James Parkes <small> USA</small></h4>
					<span class="label label-default">html5/css3</span>
					<span class="label label-default">javascript</span>
					<span class="label label-default">php</span>
					<span class="label label-default">illustrator</span>
					<ul style="margin-top:15px;">
						<li>web design and UX</li>
						<li>logo design</li>
						<li>image upload form</li>
					</ul>
				</div>
			</div>
		</div>


	</div>
</div>
  
<?php include ('footer.php'); ?>

<?php
/*
 _   _            _ _   _     ____  _  _   
| | | | ___  __ _| | |_| |__ |___ \| || |  
| |_| |/ _ \/ _` | | __| '_ \  __) | || |_ 
|  _  |  __/ (_| | | |_| | | |/ __/|__   _|
|_| |_|\___|\__,_|_|\__|_| |_|_____|  |_| 

UAH Computer Science Department
America's Datafest Global Competition
November 4, 2013 - November 10, 2013

Problem: Access to Health Information
Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible. 

Author: 
Jarrod Parkes

Team Members: 
Andrey Biglari, Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mini Zeng

POST Parameters:
FileType - $_FILES["health24"]["type"]
FileName - $_FILES["health24"]["name"]
FileSize - ($_FILES["health24"]["size"] / 1024)/100
Device - $_REQUEST["device"]
Src Language - $_REQUEST["source"]
Dst Language - $_REQUEST["destination"]
Latitude - $_REQUEST["lat"];
Longitude - $_REQUEST["lon"];
IP Address - $_REQUEST["ip"];

*/

	// create connection
	$con = mysqli_connect("localhost","root","D@T@f3st!!!","health24");

	// check connection
	if (mysqli_connect_errno($con))
	{
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}

	if($lat != "255") {
		$query = sprintf("INSERT INTO visualization (ip, source_language, target_language, device, latitude, longitude) 
			VALUES ('%s', '%s', '%s', '%s', $lat, $lon)",
				mysql_real_escape_string($ip),
				mysql_real_escape_string($src),
				mysql_real_escape_string($dst),
				mysql_real_escape_string($device));
	}
    else {
		$query = sprintf("INSERT INTO visualization (ip, source_language, target_language, device) 
			VALUES ('%s', '%s', '%s', '%s')",
				mysql_real_escape_string($ip),
				mysql_real_escape_string($src),
				mysql_real_escape_string($dst),
				mysql_real_escape_string($device));
	}
            	
	if (!$con->query($query)) {
		printf("Errormessage: %s\n", $con->error);
	}

	/* close connection */
	$con->close();	
?>

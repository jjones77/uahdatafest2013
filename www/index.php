<!DOCTYPE html>
<html>
	<head>
		<title>Health 24</title>

		<!-- Meta -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.ico">
		
		<!-- Stylesheets -->
		<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="css/custom.css" rel="stylesheet" media="screen">

	</head>
	<body cz-shortcut-listen="true">
		<div class="container" style="width:300px;margin:0 auto;padding:0 10px;">
			<a href="http://migrant.health24.net/"><img src="img/health-24-logo.png" style="margin-top:50px;"></a>
			<form action="option.php" method="POST" id="language-form" name="language-form" class="language-form" role="form">
				<label for="language">Choose Language</label>
				<select id="language" name="language" class="language-selector">					
					<option value="af">akrikaans</option>
					<option value="sq">albanian</option>
					<option value="ar">arabic</option>
					<option value="az">azerbaijani</option>
					<option value="eu">basque</option>
					<option value="be">belarusian</option>
					<option value="bn">bengali</option>
					<option value="bg">bulgarian</option>
					<option value="ca">catalan</option>
					<option value="zh-CN">chinese (simplified)</option>
					<option value="zh-TW">chinese (traditional)</option>
					<option value="hr">croatian</option>
					<option value="cs">czech</option>
					<option value="da">danish</option>
					<option value="nl">dutch</option>
					<option value="en" selected="selected">english</option>
					<option value="eo">esperanto</option>
					<option value="et">estonian</option>
					<option value="fi">finnish</option>
					<option value="fr">french</option>
					<option value="gl">galician</option>
					<option value="de">german</option>
					<option value="el">greek</option>
					<option value="iw">hebrew</option>
					<option value="hi">hindi</option>
					<option value="hu">hungarian</option>
					<option value="is">icelandic</option>
					<option value="id">indonesian</option>
					<option value="it">italian</option>
					<option value="ja">japanese</option>
					<option value="kn">kannada</option>
					<option value="ko">korean</option>
					<option value="la">latin</option>
					<option value="lv">latvian</option>
					<option value="lt">lithuanian</option>
					<option value="mk">macedonian</option>
					<option value="ms">malay</option>
					<option value="mt">maltese</option>
					<option value="no">norwegian</option>
					<option value="pl">polish</option>
					<option value="pt">portuguese</option>
					<option value="ro">romanian</option>
					<option value="ru">russian</option>
					<option value="sr">serbian</option>
					<option value="sk">slovak</option>
					<option value="sl">slovenian</option>
					<option value="es">spanish</option>
					<option value="sw">swahili</option>
					<option value="sv">swedish</option>
					<option value="ta">tamil</option>
					<option value="te">telugu</option>
					<option value="th">thai</option>
					<option value="tr">turkish</option>
					<option value="uk">ukrainian</option>
					<option value="vi">vietnamese</option>		
				</select>
				<button type="submit" class="btn btn-primary center-btn" name="language-btn">Continue to Health 24</button>
			</form>
		</div>
		<script src="http://code.jquery.com/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>
	

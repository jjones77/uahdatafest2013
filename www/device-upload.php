<?php
/*
 _   _            _ _   _     ____  _  _   
| | | | ___  __ _| | |_| |__ |___ \| || |  
| |_| |/ _ \/ _` | | __| '_ \  __) | || |_ 
|  _  |  __/ (_| | | |_| | | |/ __/|__   _|
|_| |_|\___|\__,_|_|\__|_| |_|_____|  |_| 

UAH Computer Science Department
America's Datafest Global Competition
November 4, 2013 - November 10, 2013

Problem: Access to Health Information
Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible. 

Author: 
Jarrod Parkes

Team Members: 
Andrey Biglari, Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mini Zeng

POST Parameters:
FileType - $_FILES["health24"]["type"]
FileName - $_FILES["health24"]["name"]
FileSize - ($_FILES["health24"]["size"] / 1024)/100
Device - $_REQUEST["device"]
Src Language - $_REQUEST["source"]
Dst Language - $_REQUEST["destination"]
Latitude - $_REQUEST["lat"]
Longitude - $_REQUEST["lon"]
IP Address - $_SERVER['REMOTE_ADDR']

*/

	// create temporary filename
	$filename = uniqid("img", true) . '.jpg';

	// create image file		
	move_uploaded_file( $_FILES['health24']['tmp_name'], "tmp/" . $filename);
	
	//remove 0's from end string
	$src = str_replace("0", "", $_REQUEST["source"]);
	$dst = str_replace("0", "", $_REQUEST["destination"]);
	
	// set device, lat, lon, and ip
	$device = $_REQUEST["device"];
	$lat = $_REQUEST["lat"];
	$lon = $_REQUEST["lon"];
	$ip = $_SERVER['REMOTE_ADDR'];
	
	// execute casper script (returns translated text)
	$output = shell_exec("/bin/casperjs /uahdatafest2013/phantom/health24v4.js " . $src . " " . $dst . " /uahdatafest2013/www/tmp/". $filename);

	// send translated text back to client
	print $output;

	// delete the image file
	unlink("tmp/". $filename);
?>

<?php include 'database-insert.php'; ?>

function getTranslation(languageTo) 
	{	
		var text= document.getElementById('translate').innerHTML;	    
		
		window.mycallback = function(response) 
		{
			document.getElementById('translate').innerHTML=response;
		}

		var languageFrom = "en";   
		var s = document.createElement("script");
		s.src = "http://api.microsofttranslator.com/V2/Ajax.svc/Translate?contenttype=text/plain&oncomplete=mycallback&appId=68D088969D79A8B23AF8585CC83EBA2A05A97651&from=" + languageFrom + "&to=" + languageTo + "&text=" + encodeURIComponent(text);
		document.getElementsByTagName("head")[0].appendChild(s);
   }
   
   function getUrlVars(aKey) {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars[aKey];
}

function readCookie(key)
{
    var result;
    return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? (result[1]) : null;
}

getTranslation(readCookie("language"));

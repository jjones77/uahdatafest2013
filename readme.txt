Health24 is a healthcare application tailored for migrants.

It is available for the Web, iOS, and Android platforms, and it has two major features:
(1) diagnosis
(2) image-text translation

The primary goal of Health24 may be described as follows:
"help migrants receive basic diagnoses and correctly identify and use over-the-counter (OTC) drugs to treat symptoms."
package uah.heath24.net;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class SelectLangActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_lang);
		/////////////////
		WebView webview = (WebView)findViewById(R.id.WebViewLang);
		webview.setWebChromeClient(new WebChromeClient()); 
		webview.setWebViewClient(new WebViewClient(){

		    @Override
		    public boolean shouldOverrideUrlLoading(WebView view, String url){
		      //view.loadUrl(url);
		    	Intent intent = new Intent(SelectLangActivity.this, MainActivity.class);
		        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);   
		        startActivity(intent);;
		        
		        
		      return true;
		    }
		    @Override
		    public void onLoadResource(WebView  view, String  url){
		        if( url.equals("http://migrant.health24.net/option.php") ){
		        	Log.i("info","gotcha!");
		        	Intent intent = new Intent(SelectLangActivity.this, MainActivity.class);
			        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);   
			        startActivity(intent);;
		        }
		    }
	        
		    });
		
		webview.getSettings().setDomStorageEnabled(true);
		webview.getSettings().setDatabaseEnabled(true);
		webview.getSettings().setAppCacheEnabled(true);
		webview.getSettings().setJavaScriptEnabled(true);
		
		webview.loadUrl("http://migrant.health24.net");
		
		CookieManager cookieManager = CookieManager.getInstance();
        final String cookie = cookieManager.getCookie("http://migrant.health24.net");
        Log.i("cookie", " " + cookie);
        cookie.replace("language=id", "language=es");
        cookieManager.setCookie("http://migrant.health24.net", cookie);
        webview.reload();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.select_lang, menu);
		return true;
	}

}

package uah.heath24.net;

import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.app.Activity;
import android.content.Intent;

public class MainActivity extends Activity {

	ListView list;
	String[] web = { " Select language", " Symptoms diagnosis ",
			" Look up drugs", " Translate a label",
			" Take a photo & Translate", " Visualization - Language Metrics",
			" About us",

	};
	Integer[] imageId = { R.drawable.lang, R.drawable.sick, R.drawable.pill,
			R.drawable.bottle, R.drawable.camera, R.drawable.globe,
			R.drawable.about

	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		CustomList adapter = new CustomList(MainActivity.this, web, imageId);
		list = (ListView) findViewById(R.id.listView1);
		list.setAdapter(adapter);
		list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// Toast.makeText(MainActivity.this, "You Clicked at "
				// +position+web[+ position], Toast.LENGTH_SHORT).show();
				if (position == 0) {// lang
					Intent intent = new Intent(MainActivity.this,
							SelectLangActivity.class);
					startActivity(intent);
				} else if (position == 1) {// symtoms
					Intent intent = new Intent(MainActivity.this,
							SelectSymtoms.class);
					startActivity(intent);
				} else if (position == 2) {// look up drugs
					Intent intent = new Intent(MainActivity.this,
							LookUpDrugActivity.class);
					startActivity(intent);
				} else if (position == 3) {// pick
					Intent intent = new Intent(MainActivity.this,
							TranslateLabel.class);
					startActivity(intent);
				} else if (position == 4) {// take photo
					Intent intent = new Intent(MainActivity.this,
							TakePhotoActivity.class);
					startActivity(intent);
				} else if (position == 5) {// visual
					Intent intent = new Intent(MainActivity.this,
							VisualActivity.class);
					startActivity(intent);
				} else {
					Intent intent = new Intent(MainActivity.this,
							AboutActivity.class);
					startActivity(intent);
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}

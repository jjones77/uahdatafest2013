package uah.heath24.net;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class AboutActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		WebView webview = (WebView) findViewById(R.id.WebView1);
		webview.setWebChromeClient(new WebChromeClient());
		webview.setWebViewClient(new WebViewClient());

		webview.getSettings().setDomStorageEnabled(true);
		webview.getSettings().setDatabaseEnabled(true);
		webview.getSettings().setAppCacheEnabled(true);
		webview.getSettings().setJavaScriptEnabled(true);

		webview.loadUrl("http://migrant.health24.net/about.php");
		// webview.reload();

		CookieManager cookieManager = CookieManager.getInstance();
		CookieSyncManager.createInstance(webview.getContext());
		final String cookie = cookieManager
				.getCookie("http://migrant.health24.net");
		Log.i("cookie", " " + cookie);
		Log.i("cookie", " " + cookie.indexOf("language=id"));
		String newCookie = cookie.replace("language=id", "language=es");
		Log.i("cookie", " " + newCookie);
		// cookieManager.setCookie("http://migrant.health24.net", newCookie);
		// CookieSyncManager.getInstance().sync();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.about, menu);
		return true;
	}

}

package uah.heath24.net;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;




import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class TranslateLabel extends Activity {

	private static int RESULT_LOAD_IMAGE = 1; 
	Uri myPicture = null; 
	
	private String IP = "migrant.health24.net";
	private int port = 2600;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_translate_label);
		////////////
		TextView tv =  (TextView)findViewById(R.id.TextViewTrans);
    	Typeface face=Typeface.createFromAsset(getAssets(),
                "fonts/verdana.ttf");

    	tv.setTypeface(face);
		/////////////
		
		 Intent i = new Intent(
                 Intent.ACTION_PICK,
                 android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

         startActivityForResult(i, RESULT_LOAD_IMAGE);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);

	    if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
	        Uri selectedImage = data.getData();
	        String[] filePathColumn = { MediaStore.Images.Media.DATA };

	        Cursor cursor = getContentResolver().query(selectedImage,
	                filePathColumn, null, null, null);
	        cursor.moveToFirst();

	        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
	        String picturePath = cursor.getString(columnIndex);
            //Toast.makeText(TranslateLabel.this, picturePath, Toast.LENGTH_SHORT).show();
            Log.i("info",picturePath);
	        cursor.close();

	        ImageView imageView = (ImageView) findViewById(R.id.ImageView1);
	        imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
	        //translate
	        try {
	        	 bm = BitmapFactory.decodeFile(picturePath);
	             //executeMultipartPost();
	             new SubmitTask().execute(bm);
	        } catch (Exception e) {
	            Log.e(e.getClass().getName(), e.getMessage());
	        }
	        
	       
	    }
	}
	private class SubmitTask extends AsyncTask<Object, Integer, Long> {
    	String s;
    	
    	@Override
        protected void onPreExecute() {
            super.onPreExecute();
            TextView tv =  (TextView)findViewById(R.id.TextViewTrans);
        	tv.setText("Please wait....");
        }
    	
    	protected Long doInBackground(Object... para) {
            Log.i("info","start async");
           
            long n =0;
            Bitmap bmp = (Bitmap) para[0];
            s = translate(bmp);
            
            Log.i("info", "after: "+bmp.getHeight());
        	return n;
        }

        protected void onProgressUpdate(Integer... progress) {
        	/*TextView tv =  (TextView)findViewById(R.id.TextViewTrans);
        	tv.setText("Please wait....");*/
        }

        protected void onPostExecute(Long result) {
        	//Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
        	Log.i("info",s);
        	TextView tv =  (TextView)findViewById(R.id.TextViewTrans);
        	
        	tv.setText(s);
        	tv.setText(Html.fromHtml(s).toString());
        	tv.setMovementMethod(new ScrollingMovementMethod());
        	//data == html data which you want to load 
        	
        	 /*WebView webview = (WebView)findViewById(R.id.TextViewTrans);
        	 webview.getSettings().setJavaScriptEnabled(true);
        	 webview.getSettings().setDefaultTextEncodingName("utf-8");
        	 String tt = "<!DOCTYPE html><head> <meta http-equiv=\"Content-Type\" " +
        			 "content=\"text/html; charset=utf-8\"> <html><head><meta http-equiv=\"content-type\" content=\"text/html; charset=windows-1250\">"+
        			 "<meta name=\"spanish press\" content=\"spain, spanish newspaper, news,economy,politics,sports\"><title></title></head><body id=\"body\">"+
        			s+"</body></html>";
        	 webview.loadDataWithBaseURL(null, s, "text/html", "UTF-8",null);*/
        }
    }
	
	Bitmap bm;
	/*public void executeMultipartPost() throws Exception {
		String url = "http://migrant.health24.net/device-upload.php";
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bm.compress(CompressFormat.JPEG, 75, bos);
            byte[] data = bos.toByteArray();
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost postRequest = new HttpPost(url);
            //ByteArrayBody bab = new ByteArrayBody(data, "xxxx.jpg");
            // File file= new File("/mnt/sdcard/forest.png");
            // FileBody bin = new FileBody(file);
            MultipartEntity reqEntity = new MultipartEntity(
                    HttpMultipartMode.BROWSER_COMPATIBLE);
            //reqEntity.addPart("uploaded", bab);
            //reqEntity.addPart("photoCaption", new StringBody("sfsdfsdf"));
            postRequest.setEntity(reqEntity);
            HttpResponse response = httpClient.execute(postRequest);
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent(), "UTF-8"));
            String sResponse;
            StringBuilder s = new StringBuilder();
 
            while ((sResponse = reader.readLine()) != null) {
                s = s.append(sResponse);
            }
            Log.i("info" , s.toString());
        } catch (Exception e) {
            // handle exception here
            Log.e(e.getClass().getName(), e.getMessage());
        }
	}
	*/
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.translate_label, menu);
		return true;
	}
	 public String translate(Bitmap bmp) {
		CookieManager cookieManager = CookieManager.getInstance();
		final String cookie = cookieManager
				.getCookie("http://migrant.health24.net");
		Log.i("cookie", " " + cookie);
		int i = cookie.indexOf("language=");
		String tem = cookie.substring(i,i+"language=id".length());
		Log.i("tem", " " + tem);
		String des =  tem.split("=")[1];
		Log.i("des", " " + des);
	try {
				
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
				//byte[] byteArray = stream.toByteArray();

				// File file = new File(jpgFilename);
				// BufferedImage img = ImageIO.read(file);

				byte[] imgBytes = stream.toByteArray();
				
				// ByteArrayOutputStream baos = new ByteArrayOutputStream();
				// ImageIO.write(img, "jpg", baos);
				// baos.flush();

				// imgBytes = baos.toByteArray();
				// baos.close();

				byte dev = 3;

				byte[] srcLang = "en000".getBytes("UTF-16");
				//byte[] dstLang = "es000".getBytes("UTF-16");
				byte[] dstLang = (des+"000").getBytes("UTF-16");
				System.out.format("SrcLength=%d, dstLength=%d\n", srcLang.length,
						dstLang.length);
				ByteBuffer buf = ByteBuffer.allocate(1 + srcLang.length
						+ dstLang.length + 4 + imgBytes.length);
				System.out.format("HeaderSize=%d\n", buf.capacity());
				buf.put(dev);
				buf.put(srcLang);
				buf.put(dstLang);
				buf.putInt(imgBytes.length);
				buf.put(imgBytes, 0, imgBytes.length);

				System.out.format("Sending image size=%d\n", imgBytes.length);
				Log.i("info","trying to connect....");
				Socket s = new Socket(IP, port);
				// Socket s = new Socket("146.229.232.128",2400);
				Log.i("info","connected");
				OutputStream os = s.getOutputStream();

				// send header
				buf.rewind();
				os.write(buf.array());
				os.flush();
				s.shutdownOutput();

				// read results//
				String line;
				String result = new String() ;
				BufferedReader in = new BufferedReader(new InputStreamReader(
						s.getInputStream()));
				while ((line = in.readLine()) != null) {
					System.out.println(line);
					result += new String( line.getBytes("UTF-8"));
				}
				
				s.close();
				System.out.println(result);
				return result;
			} catch (Exception e) {
				//e.printStackTrace();
				Log.e("error",e.toString());
				return null;
			}

		}

}

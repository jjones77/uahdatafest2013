/*
 _   _            _ _   _     ____  _  _
 | | | | ___  __ _| | |_| |__ |___ \| || |
 | |_| |/ _ \/ _` | | __| '_ \  __) | || |_
 |  _  |  __/ (_| | | |_| | | |/ __/|__   _|
 |_| |_|\___|\__,_|_|\__|_| |_|_____|  |_|
 
 UAH Computer Science Department
 America's Datafest
 November 1, 2013 - November 10, 2013
 
 Problem: Access to Health Information
 Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible.
 
 Author:
 Jarrod Parkes
 
 Team Members:
 Andrey Biglari, Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mini Zeng
 
 Credits:
 - PEPhotoCropEditor ( https://github.com/kishikawakatsumi/PEPhotoCropEditor )
 - ImageRotation ( http://stackoverflow.com/questions/7191492/rotate-image-in-uiimageview )
 
 */

#import "AppData.h"
#import "ImageViewController.h"
#import "HomeViewController.h"
#import "LangTableController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

@synthesize setLangButton;
@synthesize startBarButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.setLangButton.titleLabel.text = NSLocalizedString(@"Set Languages", @"set language button (iOS/iPad)");
    self.startBarButton.title = NSLocalizedString(@"Start", @"start button");
}

- (void)viewWillAppear:(BOOL)animated {
    [self.optionsTable reloadData];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // working variables
    NSMutableString *temp = [NSMutableString alloc];
    temp = [temp init];
    NSUInteger length;

    // set source language code
    AppData *appData = [AppData getInstance];
    [temp setString:[NSString stringWithFormat:@"%@", [appData->languageCodes objectAtIndex:appData->sourceIndex]]];
    length = [temp length];
    
    for(int i = 0; i < 5 - length; i++) {
        [temp appendString:@"0"];
    }
    appData->sourceCode = [NSString stringWithString:temp];
    
    // set destination language code
    [temp setString:@""];
    [temp setString:[NSString stringWithFormat:@"%@", [appData->languageCodes objectAtIndex:appData->destIndex]]];length = [temp length];
    
    for(int i = 0; i < 5 - length; i++) {
        [temp appendString:@"0"];
    }
    appData->destCode = [NSString stringWithString:temp];
}

#pragma mark - UITableViewDataSource and Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *langCellIdentifier = @"Language";
    
    AppData *appData = [AppData getInstance];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:langCellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:langCellIdentifier];
    }
    
    switch(indexPath.row) {
        case 0:
            cell.textLabel.text = NSLocalizedString(@"Source", @"source language label");
            cell.detailTextLabel.text = appData->languageArray[appData->sourceIndex];
            break;
        case 1:
            cell.textLabel.text = NSLocalizedString(@"Destination", @"source language label");
            cell.detailTextLabel.text = appData->languageArray[appData->destIndex];
            break;
    }
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LangTableController *viewController = [[LangTableController alloc] initWithStyle:UITableViewStyleGrouped];

    [viewController setTableType:indexPath.row];
    
    [self.navigationController pushViewController:viewController animated:YES];
    [self.optionsTable deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *title = NSLocalizedString(@"Language Settings", @"language settings");
    return title;
}

@end

/*
 _   _            _ _   _     ____  _  _
 | | | | ___  __ _| | |_| |__ |___ \| || |
 | |_| |/ _ \/ _` | | __| '_ \  __) | || |_
 |  _  |  __/ (_| | | |_| | | |/ __/|__   _|
 |_| |_|\___|\__,_|_|\__|_| |_|_____|  |_|
 
 UAH Computer Science Department
 America's Datafest
 November 1, 2013 - November 10, 2013
 
 Problem: Access to Health Information
 Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible.
 
 Author:
 Jarrod Parkes
 
 Team Members:
 Andrey Biglari, Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mini Zeng
 
 Credits:
 - PEPhotoCropEditor ( https://github.com/kishikawakatsumi/PEPhotoCropEditor )
 - ImageRotation ( http://stackoverflow.com/questions/7191492/rotate-image-in-uiimageview )
 
 */

#import "AppData.h"
#import "LoadingViewController.h"

@interface LoadingViewController ()

@end

@implementation LoadingViewController

@synthesize loadingImageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    [self.navigationItem setHidesBackButton:YES];
    
    AppData *appData = [AppData getInstance];
    connection = [[NSURLConnection alloc] initWithRequest:appData->request delegate:self];
    
    // Load images
    NSArray *imageNames = @[@"loading-animation_30.png", @"loading-animation_40.png", @"loading-animation_50.png",
                            @"loading-animation_60.png", @"loading-animation_70.png", @"loading-animation_80.png",
                            @"loading-animation_90.png", @"loading-animation_100.png"];
    
    NSMutableArray *images = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < imageNames.count; i++) {
        [images addObject:[UIImage imageNamed:[imageNames objectAtIndex:i]]];
    }
    for (int j = (int)imageNames.count - 1; j >= 1; j--) {
        [images addObject:[images objectAtIndex:j-1]];  // -1 since arrays are 0-based
    }
    
    loadingImageView.frame = CGRectMake(112, 162, 96, 96);
    loadingImageView.contentMode = UIViewContentModeScaleAspectFit;
    loadingImageView.animationImages = images;
    loadingImageView.animationDuration = 2.25;
    [loadingImageView startAnimating];
    
    progress = 0.0;
    
    float timer = 0.5;
    NSString *myString = NSLocalizedString(@"Starting Health24 Engine...", @"starting health24 engine");
    [NSTimer scheduledTimerWithTimeInterval:timer target:self selector:@selector(startAfterInterval:) userInfo:myString repeats:NO];
    
    timer = 2.0;
    myString = NSLocalizedString(@"Recognizing Characters...", @"recognizing characters");
    [NSTimer scheduledTimerWithTimeInterval:timer target:self selector:@selector(startAfterInterval:) userInfo:myString repeats:NO];
    
    timer = 5.0;
    myString = NSLocalizedString(@"Translating Text...", @"translating text");
    [NSTimer scheduledTimerWithTimeInterval:timer target:self selector:@selector(startAfterInterval:) userInfo:myString repeats:NO];
    
    timer = 9.0;
    myString = NSLocalizedString(@"Buffering...", @"buffering");
    [NSTimer scheduledTimerWithTimeInterval:timer target:self selector:@selector(startAfterInterval:) userInfo:myString repeats:NO];
    
    timer = 30.0;
    myString = NSLocalizedString(@"Timed Out...", @"timed out");
    [NSTimer scheduledTimerWithTimeInterval:timer target:self selector:@selector(timedOut:) userInfo:myString repeats:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    AppData *appData = [AppData getInstance];
    appData->gotResult = FALSE;
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) startAfterInterval:(NSTimer*)timer {
    self.translatingTextLabel.text = [timer userInfo];
    progress += 0.25;
    [self updateProgressBar:progress];
}

- (void) timedOut:(NSTimer*)timer {
    
    AppData *appData = [AppData getInstance];
    
    if(!appData->gotResult) {
        appData->result = nil;
        [connection cancel];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Could Not Translate", @"could not translate title")
                                                        message:NSLocalizedString(@"Translate Error Message", @"translate error message")
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"okay button")
                                              otherButtonTitles:nil];
        [alert show];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)updateProgressBar:(CGFloat) value {
	self.progressBar.progress = value;
}

#pragma mark - NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    AppData *appData = [AppData getInstance];

    // get response
    [responseData appendData:data];
    NSString* temp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    //NSLog(@"DidRecieveData: %@", temp);

    // set response
    appData->result = temp;
    appData->gotResult = TRUE;
    
    // push on results view
    [self performSegueWithIdentifier: @"Results" sender:self];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
}

@end

/*
 _   _            _ _   _     ____  _  _
 | | | | ___  __ _| | |_| |__ |___ \| || |
 | |_| |/ _ \/ _` | | __| '_ \  __) | || |_
 |  _  |  __/ (_| | | |_| | | |/ __/|__   _|
 |_| |_|\___|\__,_|_|\__|_| |_|_____|  |_|
 
 UAH Computer Science Department
 America's Datafest
 November 1, 2013 - November 10, 2013
 
 Problem: Access to Health Information
 Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible.
 
 Author:
 Jarrod Parkes
 
 Team Members:
 Andrey Biglari, Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mini Zeng
 
 Credits:
 - PEPhotoCropEditor ( https://github.com/kishikawakatsumi/PEPhotoCropEditor )
 - ImageRotation ( http://stackoverflow.com/questions/7191492/rotate-image-in-uiimageview )
 
 */

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#define TESTING_SERVICE 0

#if TESTING_SERVICE
    #define HOST @"http://migrant.health24.net/device-upload.php"
#else
    #define HOST @"http://migrant.health24.net/device-upload.php"
#endif

@interface AppData : NSObject {
    
    @public
    int sourceIndex;
    NSString *sourceCode;
    int destIndex;
    NSString *destCode;
    NSString *result;
    BOOL locationAvailable;
    BOOL gotResult;
    CLLocationDegrees longitude;
    CLLocationDegrees latitude;
    NSMutableURLRequest *request;
    NSArray *languageArray;
    NSArray *languageCodes;
}

@property(nonatomic,retain)NSString *str;

+(AppData*)getInstance;

@end

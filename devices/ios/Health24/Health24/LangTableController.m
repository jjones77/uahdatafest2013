/*
 _   _            _ _   _     ____  _  _
 | | | | ___  __ _| | |_| |__ |___ \| || |
 | |_| |/ _ \/ _` | | __| '_ \  __) | || |_
 |  _  |  __/ (_| | | |_| | | |/ __/|__   _|
 |_| |_|\___|\__,_|_|\__|_| |_|_____|  |_|
 
 UAH Computer Science Department
 America's Datafest
 November 1, 2013 - November 10, 2013
 
 Problem: Access to Health Information
 Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible.
 
 Author:
 Jarrod Parkes
 
 Team Members:
 Andrey Biglari, Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mini Zeng
 
 Credits:
 - PEPhotoCropEditor ( https://github.com/kishikawakatsumi/PEPhotoCropEditor )
 - ImageRotation ( http://stackoverflow.com/questions/7191492/rotate-image-in-uiimageview )
 
 */

#import "AppData.h"
#import "LangTableController.h"

@interface LangTableController ()

@end

@implementation LangTableController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Select Language", @"select a language title bar");
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setTableType:(int)tableType
{
    m_tableType = tableType;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    AppData *appData = [AppData getInstance];
    NSArray *values = appData->languageArray;
    return [values count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppData *appData = [AppData getInstance];
    
    static NSString *CellIdentifier = @"LangCellIdentifier";
    
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSString *s = (NSString *)[appData->languageArray objectAtIndex:indexPath.row];
    cell.textLabel.text = s;
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    if(m_tableType == 0) {
        if(indexPath.row == appData->sourceIndex) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
    }
    else {
        if(indexPath.row == appData->destIndex) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
    }

	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppData *appData = [AppData getInstance];

    if(m_tableType == 0) {
        appData->sourceIndex = indexPath.row;
        appData->sourceCode = appData->languageCodes[indexPath.row];
    }
    else {
        appData->destIndex = indexPath.row;
        appData->destCode = appData->languageCodes[indexPath.row];
    }
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

@end

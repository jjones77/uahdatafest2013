/*
 _   _            _ _   _     ____  _  _
 | | | | ___  __ _| | |_| |__ |___ \| || |
 | |_| |/ _ \/ _` | | __| '_ \  __) | || |_
 |  _  |  __/ (_| | | |_| | | |/ __/|__   _|
 |_| |_|\___|\__,_|_|\__|_| |_|_____|  |_|
 
 UAH Computer Science Department
 America's Datafest
 November 1, 2013 - November 10, 2013
 
 Problem: Access to Health Information
 Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible.
 
 Author:
 Jarrod Parkes
 
 Team Members:
 Andrey Biglari, Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mini Zeng
 
 Credits:
 - PEPhotoCropEditor ( https://github.com/kishikawakatsumi/PEPhotoCropEditor )
 - ImageRotation ( http://stackoverflow.com/questions/7191492/rotate-image-in-uiimageview )
 
 */

#import "AppData.h"

@implementation AppData

@synthesize str;

static AppData *instance = nil;

+(AppData *)getInstance
{
    @synchronized(self)
    {
        if(instance==nil)
        {
            
            instance = [AppData new];
        }
    }
    return instance;
}

- (id)init
{
    NSArray *languagesToLoad = [[NSArray alloc] initWithObjects:NSLocalizedString(@"Akrikaans", @"refers to Akrikaans language"), NSLocalizedString(@"Albanian", @"refers to Albanian language"), NSLocalizedString(@"Arabic", @"refers to Arabic language"), NSLocalizedString(@"Azerbaijani", @"refers to Azerbaijani language"), NSLocalizedString(@"Basque", @"refers to Basque language"), NSLocalizedString(@"Belarusian", @"refers to Belarusian language"), NSLocalizedString(@"Bengali", @"refers to Bengali language"), NSLocalizedString(@"Bulgarian", @"refers to Bulgarian language"), NSLocalizedString(@"Catalan", @"refers to Catalan language"), NSLocalizedString(@"Chinese (Simplified)", @"refers to Chinese (Simplified) language"), NSLocalizedString(@"Chinese (Traditional)", @"refers to Chinese (Traditional) language"), NSLocalizedString(@"Croatian", @"refers to Croatian language"), NSLocalizedString(@"Czech", @"refers to Czech language"), NSLocalizedString(@"Danish", @"refers to Danish language"), NSLocalizedString(@"Dutch", @"refers to Dutch language"), NSLocalizedString(@"English", @"refers to English language"), NSLocalizedString(@"Esperanto", @"refers to Esperanto language"), NSLocalizedString(@"Estonian", @"refers to Estonian language"), NSLocalizedString(@"Finnish", @"refers to Finnish language"), NSLocalizedString(@"French", @"refers to French language"), NSLocalizedString(@"Galician", @"refers to Galician language"), NSLocalizedString(@"German", @"refers to German language"), NSLocalizedString(@"Greek", @"refers to Greek language"), NSLocalizedString(@"Hebrew", @"refers to Hebrew language"), NSLocalizedString(@"Hindi", @"refers to Hindi language"), NSLocalizedString(@"Hungarian", @"refers to Hungarian language"), NSLocalizedString(@"Icelandic", @"refers to Icelandic language"), NSLocalizedString(@"Indonesian", @"refers to Indonesian language"), NSLocalizedString(@"Italian", @"refers to Italian language"), NSLocalizedString(@"Japanese", @"refers to Japanese language"), NSLocalizedString(@"Kannada", @"refers to Kannada language"), NSLocalizedString(@"Korean", @"refers to Korean language"), NSLocalizedString(@"Latin", @"refers to Latin language"), NSLocalizedString(@"Latvian", @"refers to Latvian language"), NSLocalizedString(@"Lithuanian", @"refers to Lithuanian language"), NSLocalizedString(@"Macedonian", @"refers to Macedonian language"), NSLocalizedString(@"Malay", @"refers to Malay language"), NSLocalizedString(@"Maltese", @"refers to Maltese language"), NSLocalizedString(@"Norwegian", @"refers to Norwegian language"), NSLocalizedString(@"Polish", @"refers to Polish language"), NSLocalizedString(@"Portuguese", @"refers to Portuguese language"), NSLocalizedString(@"Romanian", @"refers to Romanian language"), NSLocalizedString(@"Russian", @"refers to Russian language"), NSLocalizedString(@"Serbian", @"refers to Serbian language"), NSLocalizedString(@"Slovak", @"refers to Slovak language"), NSLocalizedString(@"Slovenian", @"refers to Slovenian language"), NSLocalizedString(@"Spanish", @"refers to Spanish language"), NSLocalizedString(@"Swahili", @"refers to Swahili language"), NSLocalizedString(@"Swedish", @"refers to Swedish language"), NSLocalizedString(@"Tamil", @"refers to Tamil language"), NSLocalizedString(@"Telugu", @"refers to Telugu language"), NSLocalizedString(@"Thai", @"refers to Thai language"), NSLocalizedString(@"Turkish", @"refers to Turkish language"), NSLocalizedString(@"Ukrainian", @"refers to Ukrainian language"), NSLocalizedString(@"Vietnamese", @"refers to Vietnamese language"), nil];
    
    languageArray = languagesToLoad;

    NSArray *codesToLoad = [[NSArray alloc] initWithObjects:@"af", @"sq", @"ar", @"az", @"eu", @"be", @"bn", @"bg", @"ca", @"zh-CN", @"zh-TW", @"hr", @"cs", @"da", @"nl", @"en", @"eo", @"et", @"fi", @"fr", @"gl", @"de", @"el", @"iw", @"hi", @"hu", @"is", @"id", @"it", @"ja", @"kn", @"ko", @"la", @"lv", @"lt", @"mk", @"ms", @"mt", @"no", @"pl", @"pt", @"ro", @"ru", @"sr", @"sk", @"sl", @"es", @"sw", @"sv", @"ta", @"te", @"th", @"tr", @"uk", @"vi", nil];

    languageCodes = codesToLoad;
    
    sourceIndex = 15;
    
    return self;
}

@end
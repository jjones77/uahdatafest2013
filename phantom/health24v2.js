/*
 _   _            _ _   _     ____  _  _   
| | | | ___  __ _| | |_| |__ |___ \| || |  
| |_| |/ _ \/ _` | | __| '_ \  __) | || |_ 
|  _  |  __/ (_| | | |_| | | |/ __/|__   _|
|_| |_|\___|\__,_|_|\__|_| |_|_____|  |_| 

UAH Computer Science Department
America's Datafest
November 1, 2013 - November 3, 2013

Problem: Access to Health Information
Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible. 

Author: 
Jarrod Parkes

Team Members: 
Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mimi Zeng

Credits:
- PhantomJS
- CasperJS

Purpose:
- use free OCR form to detect text from image
- translate detected text into target language
- return translated text

Needs Improvement:
- speed
- check if file does not exist

*/

/*
var test = this.evaluate(function() {
			this.fillSelectors('form#form-ocr', { 'select[name="1"]' : "chi_sim" }, true); 
		});
*/

// declare plug-ins
var system = require('system')
var fs = require('fs');

// setup casperJS paths
//phantom.casperPath = 'C:\\Program Files (x86)\\CasperJS';
//phantom.injectJs(phantom.casperPath + '\\bin\\bootstrap.js');
//phantom.casperPath = 'uahdatafest//phantom//casperjs';
//phantom.injectJs(phantom.casperPath + '//bin//bootstrap.js');
var casper = require('casper').create({
	verbose: true,
	logLevel: "debug"
});

casper.options.waitTimeout = 100000;

// declare variables
var url, source, target;
var ocrText, translatedText;

// create language map (OCR codes to google codes)
var dict = {};
dict['af'] = 'afr';			// akrikaans
dict['sq'] = 'sqi'; 		// albanian
dict['ar'] = 'ara'; 		// arabic
dict['az'] = 'aze'; 		// azerbaijani
dict['eu'] = 'eus'; 		// basque
dict['be'] = 'bel'; 		// belarusian
dict['bn'] = 'ben'; 		// bengali
dict['bg'] = 'bul'; 		// bulgarian
dict['ca'] = 'cat'; 		// catalan
dict['zn-CH'] = 'chi_sim' 	// chinese, simplified
dict['zn-TW'] = 'chi_tra';	// chinese, traditional
dict['hr'] = 'hrv'			// croatian
dict['cs'] = 'ces'			// czech
dict['da'] = 'dan'			// danish
dict['nl'] = 'nld'			// dutch
dict['en'] = 'eng';			// english
dict['eo'] = 'epo'			// esperanto
dict['et'] = 'est'			// estonian
dict['fi'] = 'fin'			// finnish
dict['fr'] = 'fra'			// french
dict['gl'] = 'glg'			// galician
dict['de'] = 'deu'			// german
dict['el'] = 'ell'			// greek
dict['iw'] = 'heb'			// hebrew
dict['hi'] = 'hin'			// hindi
dict['hu'] = 'hun'			// hungarian
dict['is'] = 'isl'			// icelandic
dict['id'] = 'ind'			// indonesian
dict['it'] = 'ita'			// italian
dict['ja'] = 'jpn'			// japanese
dict['kn'] = 'kan'			// kannada
dict['ko'] = 'kor'			// korean
dict['la'] = 'lat_lid'		// latin
dict['lv'] = 'lav'			// latvian
dict['lt'] = 'lit'			// lithuanian
dict['mk'] = 'mkd'			// macedonian
dict['ms'] = 'msa'			// malay
dict['mt'] = 'mlt'			// maltese
dict['no'] = 'nor'			// norwegian
dict['pl'] = 'pol'			// polish
dict['pt'] = 'por'			// portuguese
dict['ro'] = 'ron'			// romanian
dict['ru'] = 'rus'			// russian
dict['sr'] = 'srp'			// serbian
dict['sk'] = 'slk'			// slovak
dict['sl'] = 'slv'			// slovenian
dict['es'] = 'spa';			// spanish
dict['sw'] = 'swa'			// swahili
dict['sv'] = 'swe'			// swedish
dict['ta'] = 'tam'			// tamil
dict['te'] = 'tel'			// telugu
dict['th'] = 'tha'			// thai
dict['tr'] = 'tur'			// turkish
dict['uk'] = 'ukr'			// ukrainian
dict['vi'] = 'vie'			// vietnamese

// declare 
var page = require('webpage').create();
page.settings.resourceTimeout = 30000; // 15 seconds

// check command line arguments
if (system.args.length < 4) {
	console.log('Usage: health24.js source target imagepath');
	console.log('Example: health24.js en de //home//jarrod//Pictures//drug_label.jpeg');
	console.log('Note: imagepath requires double slashes');
	phantom.exit(1);
}
else {
	//console.log('Src: ' + dict[system.args[1]]);
	//console.log('Dst: ' + dict[system.args[2]]);
} 

// 1. open OCR website, load image, click preview	
casper.start('http://www.newocr.com/', function() {
	//console.log(test);
	this.page.uploadFile('input[type="file"]', system.args[3]);
	//this.capture('health1.png');
	this.click('button[name="preview"]');
	// this.evaluate(function() {
		// document.getElementById('language').value = dict[system.args[1]];
	// });
});

//casper.then(function() {
	//casper.fill('form', { 1 : dict[system.args[1]] }, true);
	// this.capture('health1.png');
	// this.click('button[name="preview"]');
//});

// 2. wait for new page, then click 'OCR' button
casper.waitFor(function check() {
    return this.evaluate(function() {
        return document.querySelectorAll('button').length > 2;
    });}, function then() {
		//this.capture('health2.png');
		this.click('button[name="ocr"]');
});

// 3. wait for new page
casper.waitUntilVisible('#ocr-result', function() {
    console.log("here");
});
 
// 4. scrape the resulting text
casper.then(function() {
	ocrText = this.evaluate(function() {
		return document.querySelector('#ocr-result').innerText;
	});

	console.log(ocrText);
});
/*
// 5. open Google translate using source language, target language, and OCR text
casper.then(function() {
	source = system.args[1];
	target = system.args[2];

	url = encodeURI('http://translate.google.com/#' + source + '/' + target + '/');
	
	this.page.open(url, function (status) {
		if (status !== 'success') {
			console.log('Unable to access network');
		}
	});
});

// 6.
casper.then(function() {
	console.log("here again");
//	this.capture('health4.png');
});

// 7.
casper.then(function() {
	this.fill('form[action="/"]', { text: ocrText }, true);
});

// 8. scrape the translated text
casper.waitUntilVisible('#result_box', function() {
	translatedText = this.evaluate(function () {
		return document.querySelector('#result_box').innerText;
	});
});

// 9.
casper.then(function() {
	//this.capture('health5.png');
	console.log(translatedText);
});

casper.then(function() {
	var d = new Date();
	var n = d.getTime();
	fs.write(n + '.html', '<html><head><meta charset="UTF-8"></head><body>' + translatedText + '</body></html>', 'w');
});
*/
// run steps 1-9
casper.run();

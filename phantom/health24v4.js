/*
 _   _            _ _   _     ____  _  _   
| | | | ___  __ _| | |_| |__ |___ \| || |  
| |_| |/ _ \/ _` | | __| '_ \  __) | || |_ 
|  _  |  __/ (_| | | |_| | | |/ __/|__   _|
|_| |_|\___|\__,_|_|\__|_| |_|_____|  |_| 

UAH Computer Science Department
America's Datafest Global Competition
November 4, 2013 - November 10, 2013

Problem: Access to Health Information
Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible. 

Author: 
Jarrod Parkes

Team Members: 
Andrey Biglari, Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mimi Zeng

Credits:
- PhantomJS
- CasperJS

Purpose:
- use free OCR form to detect text from image
- translate detected text into target language
- return translated text

Needs Improvement:
- speed
- check if file does not exist

*/

// declare plug-ins
var system = require('system')
var fs = require('fs');

// are we using windows system? (debug)
if(fs.separator == "\\") {
	phantom.casperPath = 'C:\\Program Files (x86)\\CasperJS';
	phantom.injectJs(phantom.casperPath + '\\bin\\bootstrap.js');
}

// include casper
var casper = require('casper').create();
casper.options.waitTimeout = 30000;

// declare variables
var url, source, target, imgpath;
var ocrText, translatedText;

// create language map (OCR codes to google codes)
var dict = {};
dict['af'] = 'afr';			// akrikaans
dict['sq'] = 'sqi'; 		// albanian
dict['ar'] = 'ara'; 		// arabic
dict['az'] = 'aze'; 		// azerbaijani
dict['eu'] = 'eus'; 		// basque
dict['be'] = 'bel'; 		// belarusian
dict['bn'] = 'ben'; 		// bengali
dict['bg'] = 'bul'; 		// bulgarian
dict['ca'] = 'cat'; 		// catalan
dict['zh-CN'] = 'chi_sim'; 	// chinese, simplified
dict['zh-TW'] = 'chi_tra';	// chinese, traditional
dict['hr'] = 'hrv';			// croatian
dict['cs'] = 'ces';			// czech
dict['da'] = 'dan';			// danish
dict['nl'] = 'nld';			// dutch
dict['en'] = 'eng';			// english
dict['eo'] = 'epo';			// esperanto
dict['et'] = 'est';			// estonian
dict['fi'] = 'fin';			// finnish
dict['fr'] = 'fra';			// french
dict['gl'] = 'glg';			// galician
dict['de'] = 'deu';			// german
dict['el'] = 'ell';			// greek
dict['iw'] = 'heb';			// hebrew
dict['hi'] = 'hin';			// hindi
dict['hu'] = 'hun';			// hungarian
dict['is'] = 'isl';			// icelandic
dict['id'] = 'ind';			// indonesian
dict['it'] = 'ita';			// italian
dict['ja'] = 'jpn';			// japanese
dict['kn'] = 'kan';			// kannada
dict['ko'] = 'kor';			// korean
dict['la'] = 'lat_lid';		// latin
dict['lv'] = 'lav';			// latvian
dict['lt'] = 'lit';			// lithuanian
dict['mk'] = 'mkd';			// macedonian
dict['ms'] = 'msa';			// malay
dict['mt'] = 'mlt';			// maltese
dict['no'] = 'nor';			// norwegian
dict['pl'] = 'pol';			// polish
dict['pt'] = 'por';			// portuguese
dict['ro'] = 'ron';			// romanian
dict['ru'] = 'rus';			// russian
dict['sr'] = 'srp';			// serbian
dict['sk'] = 'slk';			// slovak
dict['sl'] = 'slv';			// slovenian
dict['es'] = 'spa';			// spanish
dict['sw'] = 'swa';			// swahili
dict['sv'] = 'swe';			// swedish
dict['ta'] = 'tam';			// tamil
dict['te'] = 'tel';			// telugu
dict['th'] = 'tha';			// thai
dict['tr'] = 'tur';			// turkish
dict['uk'] = 'ukr';			// ukrainian
dict['vi'] = 'vie';			// vietnamese

var page = require('webpage').create();
page.settings.resourceTimeout = 30000; // 30 seconds

// check command line arguments
if (system.args.length < 4) {
	console.log('Usage: [casperjs] health24v4.js source target imagepath');
	console.log('Example: [casperjs] health24v4.js en de drug_label.jpeg');
	phantom.exit(1);
}
else {
	source = system.args[system.args.length - 3];
	target = system.args[system.args.length - 2];
	imgpath = system.args[system.args.length - 1];	
} 
  
// 1. open OCR website, load image, click preview	
casper.start('http://www.newocr.com/', function() {
	this.page.uploadFile('input[type="file"]', imgpath);
	//this.echo(dict[source]);
	this.fill('form[action="http://www.newocr.com/"]', { l: dict[source] }, true);
	this.click('button[name="preview"]');
});

// 2. wait for new page, then click 'OCR' button
casper.waitFor(function check() {
    return this.evaluate(function() {
        return document.querySelectorAll('button').length > 2;
    });}, function then() {
		this.click('button[name="ocr"]');
});

// 3. wait for new page, select resulting text
casper.waitUntilVisible('#ocr-result', function() {
    ocrText = this.evaluate(function() {
		return document.querySelector('#ocr-result').innerText;
	});
});

// 4. open Google translate using source language, target language, and OCR text
casper.then(function() {
	url = encodeURI('http://translate.google.com/#' + source + '/' + target + '/');
	
	this.page.open(url, function (status) {
		if (status !== 'success') {
			console.log('Unable to access network');
		}
	});
});

// 5. insert ocrText into source text box
casper.then(function() {
	this.fill('form[action="/"]', { text: ocrText }, true);
});

// 6. scrape the translated text
casper.waitUntilVisible('#result_box', function() {
	translatedText = this.evaluate(function () {
		return document.querySelector('#result_box').innerText;
	});
});

// 7. output translated text to console
casper.then(function() {
	console.log(translatedText);
});

// start casper! run the steps!
casper.run();

// [not needed! web layer will delete the processed image]
// casper.then(function() {
// 	fs.remove(imgpath);
// });
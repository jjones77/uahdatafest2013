/*
 _   _            _ _   _     ____  _  _   
| | | | ___  __ _| | |_| |__ |___ \| || |  
| |_| |/ _ \/ _` | | __| '_ \  __) | || |_ 
|  _  |  __/ (_| | | |_| | | |/ __/|__   _|
|_| |_|\___|\__,_|_|\__|_| |_|_____|  |_| 

UAH Computer Science Department
America's Datafest
November 1, 2013 - November 3, 2013

Problem: Access to Health Information
Description: Many migrants are among the nearly 90% of adults have difficulty comprehending health information. This is a particular challenge for migrants. [Build] An app or website that makes health information more accessible. 

Author: 
Jarrod Parkes

Team Members: 
Ha Giang, Prabhash Jha, Josh Jones, James Parkes, Jarrod Parkes, and Mimi Zeng

Credits:
- PhantomJS
- CasperJS

Purpose:
- use free OCR form to detect text from image
- translate detected text into target language
- return translated text

Needs Improvement:
- modify OCR form detection language (right now assuming English text images)
- insert text into Google translate instead of encoding in URL
- output text into HTML file

*/

// setup casperJS paths
phantom.casperPath = 'C:\\Program Files (x86)\\CasperJS';
phantom.injectJs(phantom.casperPath + '\\bin\\bootstrap.js');

// declare variables
var system = require('system')
var page = require('webpage').create();
var casper = require('casper').create();
var url, source, target;
var ocrText;

// check command line arguments
if (system.args.length < 4) {
	console.log('Usage: health24.js source target imagepath');
	console.log('Example: health24.js en de //home//jarrod//Pictures//drug_label.jpeg');
	console.log('Note: imagepath requires double slashes');
	phantom.exit(1);
} 

// 1. open OCR website, load image, click preview	
casper.start('http://www.newocr.com/', function() {
	this.page.uploadFile('input[type="file"]', system.args[3]);
	//this.capture('health1.png');
	this.click('button[name="preview"]');
});

// 2. wait for new page, then click 'OCR' button
casper.waitFor(function check() {
    return this.evaluate(function() {
        return document.querySelectorAll('button').length > 2;
    });}, function then() {
		//this.capture('health2.png');
		this.click('button[name="ocr"]');
});

// 3. wait for new page
casper.waitUntilVisible('#ocr-result', function() {
    //this.capture('health3.png');
});
 
// 4. scrape the resulting text
casper.then(function() {
	ocrText = this.evaluate(function() {
		return document.querySelector('#ocr-result').innerText;
	});
});

// 5. open Google translate using source language, target language, and OCR text
casper.then(function() {
	source = system.args[1];
    target = system.args[2];
	url = encodeURI('http://translate.google.com/#' + source + '/' + target + '/' + ocrText);
	
	this.page.open(url, function (status) {
		if (status !== 'success') {
			console.log('Unable to access network');
		}
		//this.capture('health4.png');
	});
});

// 6. scrape the translated text
casper.waitUntilVisible('#result_box', function() {
	var translatedText = this.evaluate(function () {
		return document.querySelector('#result_box').innerText;
	});
	//this.capture('health5.png');
	console.log(translatedText);
});

// run steps 1-6
casper.run();
package javabingtranslate;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

public class JavaBingTranslate 
{
    public static void main(String[] args) throws Exception {
       translate("en","ru","<div>i am here</div>test");
    }  
    
    public static void translate(String from, String to, String text) throws Exception 
    {        
        String appID = "68D088969D79A8B23AF8585CC83EBA2A05A97651";
        URL url = new URL("http://api.microsofttranslator.com/V2/Ajax.svc/Translate?appId=" + appID +"&from=" + from + "&to=" + to + "&text=" + java.net.URLEncoder.encode(text, "UTF-8"));
        URLConnection urlconn = url.openConnection();
        BufferedReader in = new BufferedReader(
                                new InputStreamReader(
                                urlconn.getInputStream()));
        String output="", inputLine="";        
        while ((inputLine =  in.readLine()) != null) 
            output+=inputLine;
        output = output.substring(2, output.length()-1);
        System.out.println(output);
        in.close();    
    }
}
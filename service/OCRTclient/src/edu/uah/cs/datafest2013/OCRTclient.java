//----------------------------------------------------------------------
//  _   _            _ _   _     ____  _  _
// | | | | ___  __ _| | |_| |__ |___ \| || |
// | |_| |/ _ \/ _` | | __| '_ \  __) | || |_
// |  _  |  __/ (_| | | |_| | | |/ __/|__   _|
// |_| |_|\___|\__,_|_|\__|_| |_|_____|  |_|
//
// UAH Computer Science Department
//  America's Datafest
//  November 1, 2013 - November 3, 2013
//
// Problem: Access to Health Information
//
// Description:
//   Many migrants are among the nearly 90% of adults have difficulty
//   comprehending health information. This is a particular challenge
//   for migrants. [Build] An app or website that makes health information
//   more accessible.
//
// Author:
// Josh Jones
//
// Team Members:
//  Josh Jones, James Parkes, Jarrod Parkes, Mini Zeng, Prabhash Jha, Ha Giang
//
// Purpose:
// - Provide sample client for OCRTservice, both for testing and to provide
//   to the mobile device developers.
//
//----------------------------------------------------------------------
package edu.uah.cs.datafest2013;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import javax.imageio.ImageIO;

public class OCRTclient 
{
    
    public OCRTclient(){}
    
    public static void main(String[] args) 
    {
        try
        {
            // jpg to byte array
            File file = new File("C:\\Users\\jjones\\Desktop\\datafest\\druglabel.jpg");
            BufferedImage img = ImageIO.read(file);
            
            byte[] imgBytes;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(img, "jpg", baos);
            baos.flush();
            imgBytes = baos.toByteArray();
            baos.close();            
            
            byte dev = 3;
            
            byte[] srcLang = "en000".getBytes("UTF-16");
            byte[] dstLang = "es000".getBytes("UTF-16");
            System.out.format("SrcLength=%d, dstLength=%d\n", srcLang.length, dstLang.length);
            ByteBuffer buf = ByteBuffer.allocate(1+srcLang.length+dstLang.length+4+imgBytes.length);
            System.out.format("HeaderSize=%d\n",buf.capacity());
            buf.put(dev);
            buf.put(srcLang);
            buf.put(dstLang);
            buf.putInt(imgBytes.length);
            buf.put(imgBytes, 0, imgBytes.length);
            
            System.out.format("Sending image size=%d\n", imgBytes.length);
            
            Socket s = new Socket("146.229.232.246",2400);
            //Socket s = new Socket("146.229.232.128",2400);
            OutputStream os = s.getOutputStream();

            // send header
            buf.rewind();
            os.write(buf.array());
            os.flush();            
            s.shutdownOutput();
            
            // read results//
            String line;
            BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
            while((line = in.readLine()) != null )
            {                
                System.out.println(line);
            }
            
            s.close();
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
    }
}


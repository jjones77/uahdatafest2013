//----------------------------------------------------------------------
//  _   _            _ _   _     ____  _  _
// | | | | ___  __ _| | |_| |__ |___ \| || |
// | |_| |/ _ \/ _` | | __| '_ \  __) | || |_
// |  _  |  __/ (_| | | |_| | | |/ __/|__   _|
// |_| |_|\___|\__,_|_|\__|_| |_|_____|  |_|
//
// UAH Computer Science Department
//  America's Datafest
//  November 1, 2013 - November 3, 2013
//
// Problem: Access to Health Information
//
// Description:
//   Many migrants are among the nearly 90% of adults have difficulty
//   comprehending health information. This is a particular challenge
//   for migrants. [Build] An app or website that makes health information
//   more accessible.
//
// Author:
// Josh Jones
//
// Team Members:
//  Josh Jones, James Parkes, Jarrod Parkes, Mini Zeng, Prabhash Jha, Ha Giang
//
// Purpose:
// - Provide service for mobile devices to access the OCRTranslate capabilities
//
//----------------------------------------------------------------------

package edu.uah.cs.datafest2013;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import javax.imageio.ImageIO;

public class OCRTservice implements Runnable
{
    private boolean running = true;
    private String portNum;
    private ServerSocket ss = null;
    
    public OCRTservice(String port)
    {
        this.portNum = port;
        System.out.println("Server running...");
    }

    @Override
    public void run()
    {
        running = true;
        try
        {
            ss = new ServerSocket(Integer.parseInt(portNum));            
            while( running )
            {
                Socket client = ss.accept();
                System.out.println("Client connected from: " + client.toString() );
                handleClient(client);                
            }
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }        
    }

    public void handleClient(final Socket client)
    {
        try
        {
            Thread t = new Thread() 
            {
                @Override
                public void run() 
                {
                    try
                    {                        
                        InputStream is = client.getInputStream();                        
                        
                        byte[] hdr = new byte[29];
                        is.read(hdr,0,29);
                        
                        ByteBuffer buf = ByteBuffer.wrap(hdr);
                        buf.rewind();

                        // device
                        byte dev = buf.get();
                        
                        // srcLang
                        byte srcLangB[] = new byte[12];
                        buf.get(srcLangB);
                        String srcLang = new String(srcLangB,"UTF-16").replaceAll("0", "");                        

                        // dstLang
                        byte dstLangB[] = new byte[12];
                        buf.get(dstLangB);
                        String dstLang = new String(dstLangB,"UTF-16").replaceAll("0", "");                        
                        
                        // image size
                        int imgSize = buf.getInt();
                        
                        ByteBuffer imgBuf = ByteBuffer.allocate(imgSize);
                        imgBuf.rewind();

                        // image data
                        int total=0;
                        int count;
                        byte[] block = new byte[1024];
                        while( (count=is.read(block)) > 0 )
                        {
                            total += count;
                            imgBuf.put(block, 0, count);
                        }
                        System.out.format("%d:%s:%s:%d\n", dev, srcLang, dstLang, total);
                        
                        imgBuf.rewind();
                        if ( !imgBuf.hasArray() )
                        {
                            System.out.print("No backing array");
                        }
                        ByteArrayInputStream bais = new ByteArrayInputStream(imgBuf.array());
                        BufferedImage bimg = ImageIO.read(bais);
                        
                        if ( bimg == null )
                        {
                            System.out.println("BufferedImage is null");
                        }
                        
                        File tmpFile = File.createTempFile("temp", ".jpg");
                        if ( tmpFile == null )
                        {
                            System.out.println("Temp file is null");
                        }
                        ImageIO.write(bimg, "jpg", tmpFile);
                        
                        System.out.format("Image save as %s\n", tmpFile.getAbsolutePath());
                        
                        String[] cmd = { "/bin/casperjs", "/uahdatafest2013/phantom/health24v3.js", srcLang, dstLang, tmpFile.getAbsolutePath() };
                        ProcessBuilder pb = new ProcessBuilder( cmd );                        
                        Process p = pb.start();                        
                        BufferedReader pin = new BufferedReader( new InputStreamReader( p.getInputStream() ) );
                        
                        OutputStream os = client.getOutputStream();
                        String line;
                        while((line = pin.readLine()) != null) 
                        {
                            System.out.format("%s\n",line);
                            os.write(line.getBytes("UTF-16"));
                            os.flush();
                        }
                        os.flush();
                        p.waitFor();                        
                        client.close();
                    }
                    catch( Exception e )
                    {
                        e.printStackTrace();
                    }
                }
            };
            t.start();        
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) 
    {       
        String portName;
        if ( args.length < 1 )
            portName = "2400";
        else
            portName = args[0];
        System.out.format("Starting service on port %s\n", portName);
        OCRTservice srv = new OCRTservice(portName);
        Thread t = new Thread(srv);
        t.start();        
    }
}
